
import math
import pypdt
import sys
import argparse
import numpy as np
import importlib
import itertools

import tensorflow as tf

# import the TensorflowAnalysis libraries
import Interface
import Optimisation

# import the local libraries
import MatrixElement

# import the Python settings
from python_settings import *

# compute the uncertainties of the fit fractions
def fit_fractions_uncertainties():

    
    
    return

# compute and write the fit fractions
def write_fit_fractions(sess, pdf, data_placeholder,
                        switches, norm_sample,
                        resonances, outFile_name):
    
    # compute the fit fractions and return the total PDF integral as well
    fit_fractions, total_int = Optimisation.CalculateFitFractions(sess, pdf, data_placeholder,
                                                                  switches, norm_sample)

    # get the fit fractions uncertainties
    #fit_fractions_uncertainties = MatrixElement.FitUncertainty_numerator(phsp, datapoint, resonance)
    
    # list of resonance names
    resonance_names = [res.name for res in resonances]
    
    # write the fit fractions in the output file
    Optimisation.WriteFitFractions(fit_fractions,
                                   resonance_names +  ["non-resonant"],
                                   outFile_name + "_fractions.info")

    return fit_fractions, total_int

# compute and write out the interference terms
def write_interference_terms(sess, phase_space, total_int, norm_sample,
                             resonances, outFile_name):

    # list of resonance names
    resonance_names = [res.name for res in resonances]
    
    # create all possible 2-states combinations
    res_combinations = list(itertools.combinations(resonance_names, 2))
    
    # create a dictionary of {2-states combinations : interference amplitude}
    interference_terms = {}
    
    for res in res_combinations:
        interference_terms.update({res : Interface.CastComplex(0.)})

    # now get the 2-states interference amplitudes
    for states, data_pdf_interference in interference_terms.iteritems():
        
        # get the indeces related to the current states                                                                                                                                          
        component_indeces = [resonance_names.index(states[0]),
                             resonance_names.index(states[1])]
        
        # compute the interference amplitude                                                                                                                                                     
        data_pdf_interference = MatrixElement.MatrixElement_interference(phase_space, phase_space.data_placeholder,
                                                                         component_indeces, resonances)
        
        # update the dictionary                                                                                                                                                                  
        interference_terms[states] = data_pdf_interference
        
    # compute the interference terms
    interference_terms = Optimisation.CalculateInteferenceTerms(sess, total_int, phase_space.data_placeholder,
                                                                data_pdf_interference, interference_terms,
                                                                norm_sample = norm_sample)
    
    # write the interference terms in the output file
    Optimisation.WriteInterferenceTerms(interference_terms,
                                        outFile_name + "_interference.info")


import sys

#import the local libraries
from Constants import *
import MatrixElement
import Optimisation
from Interface import Const
from Optimisation import FitParameter

# label of the data legend
data_legend = "Run I + Run II data"

# non-resonant component
non_res = MatrixElement.NonResonant(nonres_size = Const(nonres_size_VALUE),
                                    colour = kPink, linestyle = 7, fillstyle = 3954)

# list of Lc D0 resonances
A = [
]

# list of D0 K resonances
B = [
    # Lb --> Lc Ds1*(2700)
    MatrixElement.BResonanceBreitWigner(name = Ds1_2700.name, label = Ds1_2700.label,
                                        mass = Ds1_2700.mass, width = Ds1_2700.width,
                                        spin = Ds1_2700.spin, parity = Ds1_2700.parity, aboveLmin = 0,
                                        use_BLS_RealAndImag = True,
                                        couplings_dict_M = {
                                            "Ds1_2700__M_Bs1l0hall_Real"  : Const(Ds1_2700__M_Bs1l0hall_Real_VALUE),
                                            "Ds1_2700__M_Bs1l0hall_Imag"  : Const(Ds1_2700__M_Bs1l0hall_Imag_VALUE)
                                        },                                        
                                        couplings_dict_res = {
                                            "Ds1_2700__res_Bs0l2h-2_Real" : Const(Ds1_2700__res_Bs0l2h-2_Real_VALUE),
                                            "Ds1_2700__res_Bs0l2h-2_Imag" : Const(Ds1_2700__res_Bs0l2h-2_Imag_VALUE),
                                            "Ds1_2700__res_Bs0l2h0_Real"  : Const(Ds1_2700__res_Bs0l2h0_Real_VALUE),
                                            "Ds1_2700__res_Bs0l2h0_Imag"  : Const(Ds1_2700__res_Bs0l2h0_Imag_VALUE),
                                            "Ds1_2700__res_Bs0l2h2_Real"  : Const(Ds1_2700__res_Bs0l2h2_Real_VALUE),
                                            "Ds1_2700__res_Bs0l2h2_Imag"  : Const(Ds1_2700__res_Bs0l2h2_Imag_VALUE)
                                        },
                                        resdecay_parity_conservation = True, 
                                        colour = kBrown, linestyle = 7, fillstyle = 3954),

    # Lb --> Lc DsJ(3040)
    MatrixElement.BResonanceBreitWigner(name = Dsj_3040.name, label = Dsj_3040.label,
                                        mass = Dsj_3040.mass, width = Dsj_3040.width,
                                        spin = Dsj_3040.spin, parity = Dsj_3040.parity, aboveLmin = 0,
                                        use_BLS_RealAndImag = True,
                                        couplings_dict_M = {
                                            "DsJ_3040__M_Bs1l0hall_Real" : Const(DsJ_3040__M_Bs1l0hall_Real_VALUE),
                                            "DsJ_3040__M_Bs1l0hall_Imag" : Const(DsJ_3040__M_Bs1l0hall_Imag_VALUE)
                                        },
                                        couplings_dict_res = {
                                            "DsJ_3040__res_Bs0l2h-2_Real" : Const(DsJ_3040__res_Bs0l2h-2_Real_VALUE),
                                            "DsJ_3040__res_Bs0l2h-2_Imag" : Const(DsJ_3040__res_Bs0l2h-2_Imag_VALUE),
                                            "DsJ_3040__res_Bs0l2h0_Real"  : Const(DsJ_3040__res_Bs0l2h0_Real_VALUE),
                                            "DsJ_3040__res_Bs0l2h0_Imag"  : Const(DsJ_3040__res_Bs0l2h0_Imag_VALUE),
                                            "DsJ_3040__res_Bs0l2h2_Real"  : Const(DsJ_3040__res_Bs0l2h2_Real_VALUE),
                                            "DsJ_3040__res_Bs0l2h2_Imag"  : Const(DsJ_3040__res_Bs0l2h2_Imag_VALUE)
                                        },
                                        resdecay_parity_conservation = True,
                                        colour = kBlue, linestyle = 7, fillstyle = 3954),
]

# list of Lc K resonances
C = [    
    # Lb --> Xic(2790) D0
    #MatrixElement.CResonanceBreitWigner(name = Xic_2790.name, label = Xic_2790.label,
    #                                    mass = Xic_2790.mass, width = Const(0.015),
    #                                    spin = Xic_2790.spin, parity = Xic_2790.parity, aboveLmin = 0,
    #                                    use_BLS_RealAndImag = True,
    #                                    couplings_dict_M = {
    #                                        "Xic_2790__M_Bs1l0hall_Real"  : Const(Xic_2790__M_Bs1l0hall_Real_VALUE),
    #                                        "Xic_2790__M_Bs1l0hall_Imag"  : Const(Xic_2790__M_Bs1l0hall_Imag_VALUE)
    #                                    },
    #                                    couplings_dict_res = {
    #                                        "Xic_2790__res_Bs1l0h-1_Real" : Const(Xic_2790__res_Bs1l0h-1_Real_VALUE),
    #                                        "Xic_2790__res_Bs1l0h-1_Imag" : Const(Xic_2790__res_Bs1l0h-1_Imag_VALUE),
    #                                        "Xic_2790__res_Bs1l0h1_Real"  : Const(Xic_2790__res_Bs1l0h1_Real_VALUE),
    #                                        "Xic_2790__res_Bs1l0h1_Imag"  : Const(Xic_2790__res_Bs1l0h1_Imag_VALUE)
    #                                    },
    #                                    resdecay_parity_conservation = True,
    #                                    colour = kCyan, linestyle = 7, fillstyle = 3954),

    # Lb --> Xic(2815) D0
    #MatrixElement.CResonanceBreitWigner(name = Xic_2815.name, label = Xic_2815.label,
    #                                    mass = Xic_2815.mass, width = Const(0.015),
    #                                    spin = Xic_2815.spin, parity = Xic_2815.parity, aboveLmin = 0,
    #                                    use_BLS_RealAndImag = True,
    #                                    couplings_dict_M = {
    #                                        "Xic_2815__M_Bs3l2hall_Real"  : Const(Xic_2815__M_Bs3l2hall_Real_VALUE),
    #                                        "Xic_2815__M_Bs3l2hall_Imag"  : Const(Xic_2815__M_Bs3l2hall_Imag_VALUE)
    #                                    },
    #                                    couplings_dict_res = {
    #                                        "Xic_2815__res_Bs1l4h-1_Real" : Const(Xic_2815__res_Bs1l4h-1_Real_VALUE),
    #                                        "Xic_2815__res_Bs1l4h-1_Imag" : Const(Xic_2815__res_Bs1l4h-1_Imag_VALUE),
    #                                        "Xic_2815__res_Bs1l4h1_Real"  : Const(Xic_2815__res_Bs1l4h1_Real_VALUE),
    #                                        "Xic_2815__res_Bs1l4h1_Imag"  : Const(Xic_2815__res_Bs1l4h1_Imag_VALUE)
    #                                    },
    #                                    resdecay_parity_conservation = True,
    #                                    colour = kMagenta, linestyle = 7, fillstyle = 3954),
]

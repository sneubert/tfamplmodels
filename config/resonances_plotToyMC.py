
import sys

#import the local libraries
sys.path.append("../../amplitude_fits/")

from Constants import *
import MatrixElement
import Optimisation
from Interface import Const
from Optimisation import FitParameter

# label of the data legend
data_legend = "toy data"

# non-resonant component
non_res = MatrixElement.NonResonant(nonres_size = Const(nonres_size_VALUE),
                                    colour = kYellow, linestyle = 7, fillstyle = 3954)

# list of Lc D0 resonances
A = [
]

# list of D0 K resonances
B = [
    # Lb --> Lc Ds1*(2700)
    MatrixElement.BResonanceBreitWigner(name = Ds1_2700.name, label = Ds1_2700.label,
                                        mass = Ds1_2700.mass, width = Ds1_2700.width,
                                        spin = Ds1_2700.spin, parity = Ds1_2700.parity, aboveLmin = 0,
                                        use_BLS_RealAndImag = True,
                                        couplings_dict_M = {"Ds1_2700__M_Bs1l0hall_Real" : Const(Ds1_2700__M_Bs1l0hall_Real_VALUE),
                                                            "Ds1_2700__M_Bs1l0hall_Imag" : Const(Ds1_2700__M_Bs1l0hall_Imag_VALUE)},
                                        couplings_dict_res = {"Ds1_2700__res_Bs0l2h-2_Real" : Const(Ds1_2700__res_Bs0l2h-2_Real_VALUE),
                                                              "Ds1_2700__res_Bs0l2h-2_Imag" : Const(Ds1_2700__res_Bs0l2h-2_Imag_VALUE),
                                                              "Ds1_2700__res_Bs0l2h0_Real"  : Const(Ds1_2700__res_Bs0l2h0_Real_VALUE),
                                                              "Ds1_2700__res_Bs0l2h0_Imag"  : Const(Ds1_2700__res_Bs0l2h0_Imag_VALUE),
                                                              "Ds1_2700__res_Bs0l2h2_Real"  : Const(Ds1_2700__res_Bs0l2h2_Real_VALUE),
                                                              "Ds1_2700__res_Bs0l2h2_Imag"  : Const(Ds1_2700__res_Bs0l2h2_Imag_VALUE)},
                                        resdecay_parity_conservation = True,
                                        colour = kBrown, linestyle = 7, fillstyle = 3954),
]

# list of Lc K resonances
C = [
]

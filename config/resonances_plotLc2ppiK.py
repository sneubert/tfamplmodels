#####################################################################
# THIS IS A TEMPLATE FILE WHICH WILL BE FILLED WITH THE FIT RESULTS #
#####################################################################
import sys

#import the local libraries
from Constants import *
import MatrixElement
import Optimisation
from Interface import Const

# label of the data legend
data_legend = "toy data"

# non-resonant component
non_res = MatrixElement.NonResonant(nonres_size = Const(0.), colour = kGray, linestyle = 7, fillstyle = 3954)

# list of p pi resonances
A = [
    # Lc -->  Delta++ K
    MatrixElement.AResonanceBreitWigner(name = Delta1232.name, label = Delta1232.label,
                                        mass = Delta1232.mass,
                                        width = Delta1232.width,
                                        spin = Delta1232.spin, parity = Delta1232.parity, aboveLmin = 0,
                                        use_BLS_RealAndImag = True,
                                        couplings_dict_M = {"D1232__M_Bs3l2hall_Real" : Const(D1232__M_Bs3l2hall_Real_VALUE),
                                                           "D1232__M_Bs3l2hall_Imag" : Const(D1232__M_Bs3l2hall_Imag_VALUE)},
                                        couplings_dict_res = {"D1232__res_Bs1l2h1_Real" : Const(D1232__res_Bs1l2h1_Real_VALUE),
                                                              "D1232__res_Bs1l2h1_Imag" : Const(D1232__res_Bs1l2h1_Imag_VALUE),
                                                              "D1232__res_Bs1l2h-1_Real" : Const(D1232__res_Bs1l2h-1_Real_VALUE),
                                                              "D1232__res_Bs1l2h-1_Imag" : Const(D1232__res_Bs1l2h-1_Imag_VALUE)},
                                        resdecay_parity_conservation = True,colour = kBlue, linestyle = 7, fillstyle = 3954)



]

# list of pi K resonances
B = [
    # Lc --> p Kst890
    MatrixElement.BResonanceBreitWigner(name = Kst890.name, label = Kst890.label,
                                        mass = Kst890.mass, width = Kst890.width,
                                        spin = Kst890.spin, parity = Kst890.parity, aboveLmin = 0,
                                        use_BLS_RealAndImag = True,
                                        couplings_dict_M = {"Kst890__M_Bs1l0hall_Real" : Const(Kst890__M_Bs1l0hall_Real_VALUE),
                                                            "Kst890__M_Bs1l0hall_Imag" : Const(Kst890__M_Bs1l0hall_Imag_VALUE)},
                                        couplings_dict_res = {"Kst890__res_Bs0l2h-2_Real" : Const(Kst890__res_Bs0l2h-2_Real_VALUE),
                                                              "Kst890__res_Bs0l2h-2_Imag" : Const(Kst890__res_Bs0l2h-2_Imag_VALUE),
                                                              "Kst890__res_Bs0l2h0_Real"  : Const(Kst890__res_Bs0l2h0_Real_VALUE),
                                                              "Kst890__res_Bs0l2h0_Imag"  : Const(Kst890__res_Bs0l2h0_Imag_VALUE),
                                                              "Kst890__res_Bs0l2h2_Real"  : Const(Kst890__res_Bs0l2h2_Real_VALUE),
                                                              "Kst890__res_Bs0l2h2_Imag"  : Const(Kst890__res_Bs0l2h2_Imag_VALUE)},
                                        resdecay_parity_conservation = True,colour = kBrown, linestyle = 7, fillstyle = 3954),


]

# list of p K resonances
C = [
    # Lc --> Lambda*(1520) pi
    MatrixElement.CResonanceBreitWigner(name = Lambda1520.name, label = Lambda1520.label,
                                        mass = Lambda1520.mass,
                                        width = Lambda1520.width,
                                       spin = Lambda1520.spin, parity = Lambda1520.parity, aboveLmin = 0,
                                       use_BLS_RealAndImag = True,
                                       couplings_dict_M = {"L1520__M_Bs3l2hall_Real" : Const(L1520__M_Bs3l2hall_Real_VALUE),
                                                           "L1520__M_Bs3l2hall_Imag" : Const(L1520__M_Bs3l2hall_Imag_VALUE)},
                                       couplings_dict_res = {"L1520__res_Bs1l4h1_Real" : Const(L1520__res_Bs1l4h1_Real_VALUE),
                                                             "L1520__res_Bs1l4h1_Imag" : Const(L1520__res_Bs1l4h1_Imag_VALUE),
                                                             "L1520__res_Bs1l4h-1_Real" : Const(L1520__res_Bs1l4h-1_Real_VALUE),
                                                             "L1520__res_Bs1l4h-1_Imag" : Const(L1520__res_Bs1l4h-1_Imag_VALUE)},
                                       resdecay_parity_conservation = True,colour = kPink, linestyle = 7, fillstyle = 3954)


]

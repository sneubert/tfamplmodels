
import math
import pypdt
import sys
import importlib
import numpy as np

import time

import tensorflow as tf

from tensorflow.python import debug as tf_debug

from ROOT import TFile, TH1F, TCanvas

# import the local libraries
import MatrixElement
import PhaseSpace
import Fractions_Interference

# Tensorflow stuff
import Optimisation
import Interface
import ToyMC

# import the Python settings
from python_settings import *

from Constants import *
from variables_Lc2ppiK import *

import numpy
#numpy.set_printoptions(threshold=numpy.nan)

import argparse

# import the Python settings
from python_settings import *

#configuration to run over multiple threads in parallel... in principle, at least
session_conf = tf.ConfigProto(
        intra_op_parallelism_threads=4,
        inter_op_parallelism_threads=4
)


if __name__ == "__main__" :

        logging.info("")
        logging.info("--------------------------------")
        logging.info("--------- GenerateToys ---------")
        logging.info("--------------------------------")

        ###########
        ## parser
        ###########

        parser = argparse.ArgumentParser(description="Parser of the GenerateToys script")

        # get the out file and tree names
        parser.add_argument('--configFile_name', dest='configFile_name',
                            default = '../config/amplitude_fits/resonances_genToyMC.py', help="name of the config file with the resonances to simulate")
        parser.add_argument('--outFile_name', dest='outFile_name',
                            default = 'toy.root', help="name of the output toy MC file")
        parser.add_argument('--outTree_name', dest='outTree_name',
                            default = 'toy', help="name of the output toy MC tree")
        parser.add_argument('--outFile_params_name', dest='outFile_params_name',
                            default = 'out_params.info', help="proto name of the output logs containing the generated parameters")
        parser.add_argument('--random_seed', dest='random_seed',
                            default = False, help="use random seed for the toy generation?")
        parser.add_argument('--seed', dest='seed',
                            default = 1, help="set the seed used by the toy generation")
        parser.add_argument('--n_events', dest='n_events',
                            default = 50000, help = "number of signal events to generate")
        parser.add_argument('--n_events_norm', dest='n_events_norm',
                            default = 100000, help = "number of events of the normalisation sample to generate")

        # now get the parsed options
        args = parser.parse_args()

        configFile_name = args.configFile_name
        outFile_name = args.outFile_name
        outTree_name = args.outTree_name
        outFile_params_name = args.outFile_params_name
        random_seed = args.random_seed
        seed = int(args.seed)
        n_events = int(args.n_events)
        n_events_norm = int(args.n_events_norm)

        # print the parsed options
        logging.info("\n")
        logging.info("configFile_name = {}".format(configFile_name))
        logging.info("outFile_name = {}".format(outFile_name))
        logging.info("outTree_name = {}".format(outTree_name))
        logging.info("outFile_params_name = {}".format(outFile_params_name))
        logging.info("random_seed = {}".format(random_seed))
        logging.info("seed = {}".format(seed))
        logging.info("n_events = {}".format(n_events))
        logging.info("n_events_norm = {}".format(n_events_norm))

        # initialise the TensorFlow session
	sess = tf.Session(config = session_conf)
        init = tf.global_variables_initializer()
        sess.run(init)

	# set the a few options for the MatrixElement module
        MatrixElement.sess = sess

        # the same for the PhaseSpace module
        PhaseSpace.sess = sess

        ##############
        ## Generate the signal sample
        ##############

        # import the resonances from the config file
        sys.path.append(os.path.dirname(configFile_name))
        resonance_module = importlib.import_module(os.path.basename(configFile_name)[:-3])  #-3 removes the '.py' at the end of the string

        non_res = resonance_module.non_res
        A = resonance_module.A
        B = resonance_module.B
        C = resonance_module.C

        logging.info("")
        logging.info("Imported resonances:")
        logging.info("non_res = {}".format(non_res))
        logging.info("A = {}".format([A_res.name for A_res in A]))
        logging.info("B = {}".format([B_res.name for B_res in B]))
        logging.info("C = {}\n".format([C_res.name for C_res in C]))

        resonances = A + B + C

        switches = Optimisation.Switches(len(resonances) + 1)

        # define the phase space
        phase_space = PhaseSpace.ThreeBodyPhaseSpace()

        # perform an innocent test
        #phase_space.FinalStateMomenta(None, test = True)

        data_placeholder = phase_space.data_placeholder
        norm_placeholder = phase_space.norm_placeholder

        # set the matrix elements
        data_pdf = MatrixElement.MatrixElement(phase_space, data_placeholder, switches,
                                               -1, resonances, non_res)

        # generate a uniform sample
        norm_sample = sess.run(phase_space.UniformSample(n_events_norm))

        # start with the fun: estimate the maximum of the density function
	majorant = ToyMC.EstimateMaximum(sess, data_pdf, data_placeholder, norm_sample )*1.1

        # check that the majorant is positive
        if not (majorant > 0):
                logging.error("GenerateToys::Error, maximum = {}\n".format(majorant))
                exit(0)

        logging.info("\n")
        logging.info("Running the toy MC")

        # do you want a random seed?
        # This would overwrite the already set seed
        if random_seed :
                seed = int(time.clock() * 10000000.)

        logging.info("seed = {}".format(seed))

        # run the toy MC!
	data_sample = ToyMC.RunToyMC(sess, data_pdf, data_placeholder, phase_space, n_events,
                                     majorant, chunk = 100,
                                     seed = seed)

        #######

        logging.info("Saving the toy MC in {}".format(outFile_name))

        # save the output
        out_file = TFile.Open(outFile_name, "RECREATE")

        # add some additional variables to the sample
        # that's what I hate of all this pythonic magic, especially if combined with Tensorflow
        for var in other_variables:
                data_sample = numpy.c_[ data_sample, sess.run(eval("phase_space." + var[0] + "(data_sample)"))]

        # add a weight (= 1.) which will be used as sweight
        data_sample = numpy.c_[ data_sample, numpy.ones(numpy.size(data_sample, 0)) ]

        # some debug printouts
        logging.debug("m_d2d3 = {}".format(sess.run(eval("phase_space.m_d2d3(data_sample)"))))
        logging.debug("cosThetaA = {}".format(sess.run(eval("phase_space.cosThetaA(data_sample)"))))

        logging.debug("cosThetaM_B = {}".format(sess.run(eval("phase_space.cosThetaM_B(data_sample)"))))
        logging.debug("cosThetaB = {}".format(sess.run(eval("phase_space.cosThetaB(data_sample)"))))
        logging.debug("phid1_B = {}".format(sess.run(eval("phase_space.phid1_B(data_sample)"))))
        logging.debug("phid2_B = {}".format(sess.run(eval("phase_space.phid2_B(data_sample)"))))

        logging.debug("cosThetaM_C = {}".format(sess.run(eval("phase_space.cosThetaM_C(data_sample)"))))
        logging.debug("cosThetaC = {}".format(sess.run(eval("phase_space.cosThetaC(data_sample)"))))
        logging.debug("phid2_C = {}".format(sess.run(eval("phase_space.phid2_C(data_sample)"))))
        logging.debug("phid1_C = {}".format(sess.run(eval("phase_space.phid1_C(data_sample)"))))

        # write the tree of the signal generated sample
	Optimisation.FillNTuple(outTree_name, data_sample,
                                [var[1] for var in variables] + [addvar[1] for addvar in other_variables] + ["NLb_sw"])

        ##############
        ## Generate the normalisation sample
        ##############

        # generate a uniform sample
        norm_sample = sess.run(phase_space.UniformSample(n_events_norm))

        # add some additional variables to the sample
        # that's what I hate of all this pythonic magic, especially if combined with Tensorflow
        for var in other_variables:
		norm_sample = numpy.c_[ norm_sample, sess.run(eval("phase_space." + var[0] + "(norm_sample)"))]

        # add a weight (= 1.) which will be used as MC-reweighting weight
	norm_sample = numpy.c_[ norm_sample, numpy.ones(numpy.size(norm_sample, 0)) ]

        # write the tree of the normalisation sample
	Optimisation.FillNTuple(outTree_name + "_normalisation", norm_sample,
	                        [var[1] for var in variables] + [addvar[1] for addvar in other_variables] + ["weight"])

        logging.info("Toys saved to {}".format(out_file.GetName()))

        # close the file
        out_file.Close()

        #############
        ## Save out the B_LS couplings
        #############

        # print the B_LS couplings of the resonances, for debug
        for res in resonances:
                MatrixElement.print_BSL(res.BSL_M, res.name + "__M")
	        MatrixElement.print_BSL(res.BSL_res, res.name + "__res")

        # open the output files with the parameters
        outFile_params_cartesian = open(outFile_params_name + "_params_cartesian.info", "w")
        outFile_params_polar = open(outFile_params_name + "_params_polar.info", "w")

        # write out the values of the B_LS couplings
        # and the size of the non-resonant contribution
        # in both cartesian and polar coordinates
        [MatrixElement.SaveBSL_cartesian(res, outFile_params_cartesian) for res in resonances]
        non_res.SaveNonResSize(outFile_params_cartesian)

        # also save the Ds*1_2700__M_B01_Mag parameter,
        # which will be later used as scaling factor to compare the generated and fitted parameters
        # ("that's the spirit of hard-coding!", cit.)
        #for B_res in B:
        #        if B_res.name == "Ds*1_2700":
        #                outFile_params_cartesian.write("Ds*1_2700__M_B01_Mag\t" + str(sess.run(Interface.Mag(B_res.BSL_M.B_SL[0][1]))) + "\n")

        ##

        [MatrixElement.SaveBSL_polar(res, outFile_params_polar) for res in resonances]
	non_res.SaveNonResSize(outFile_params_polar)

        # close the files
        outFile_params_cartesian.close()
        outFile_params_polar.close()

        #############
        ## compute and write out the generated fit fractions and interference terms
        #############

        # total PDF integral over the normalisation sample
	#pdf_norm = sess.run(data_pdf, feed_dict = {data_placeholder : norm_sample} )
        #total_int = np.sum(pdf_norm)

        fit_fractions, total_int = Fractions_Interference.write_fit_fractions(sess, data_pdf, data_placeholder,
                                                                              switches, norm_sample,
                                                                              resonances, outFile_params_name)

        # compute the interference terms only if you have more then one resonance
        if len(resonances) > 1 :
                     Fractions_Interference.write_interference_terms(sess, phase_space, total_int, norm_sample,
                                                                     resonances, outFile_params_name)

        #############

        # close the session
        sess.close()

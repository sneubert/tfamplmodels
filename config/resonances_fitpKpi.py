
import sys

#import the local libraries
from Constants import *
import MatrixElement
import Optimisation
from Interface import Const
from Optimisation import FitParameter

# non-resonant component
non_res = MatrixElement.NonResonant(nonres_size = FitParameter("nonres_size", 1., 0., 10., 0.01),
                                    colour = kGray, linestyle = 7, fillstyle = 3954)

# label of the data legend
data_legend = "Toy MC"

# list of p pi resonances
A = [
    #Lc -->  Delta++ K
    MatrixElement.AResonanceBreitWigner(name = Delta1232.name, label = Delta1232.label,
                                        mass = Delta1232.mass,
                                        width = Delta1232.width,
                                        spin = Delta1232.spin, parity = Delta1232.parity, aboveLmin = 0,
                                        use_BLS_RealAndImag = True,
                                        couplings_dict_M = {"D1232__M_Bs3l2hall_Real" : FitParameter("D1232__M_Bs3l2hall_Real", 0.5, -10., 10., 0.01),
                                                           "D1232__M_Bs3l2hall_Imag" : FitParameter("D1232__M_Bs3l2hall_Imag", 0.5, -10., 10., 0.01),},
                                        couplings_dict_res = {"D1232__res_Bs1l2h1_Real" : FitParameter("D1232__res_Bs1l2h1_Real", 0.5, -10., 10., 0.01),
                                                              "D1232__res_Bs1l2h1_Imag" : FitParameter("D1232__res_Bs1l2h1_Imag", 0.5, -10., 10., 0.01),
                                                              "D1232__res_Bs1l2h-1_Real" : FitParameter("D1232__res_Bs1l2h-1_Real", 0.5, -10., 10., 0.01),
                                                              "D1232__res_Bs1l2h-1_Imag" : FitParameter("D1232__res_Bs1l2h-1_Imag", 0.5, -10., 10., 0.01)},
                                        resdecay_parity_conservation = True, colour = kBrown, linestyle = 7, fillstyle = 3954)


    # Lb --> Pc K
    #MatrixElement.AResonanceBreitWigner(Pc_4380.name, Pc_4380.label,
    #                                    mass = Pc_4380.mass, width = Pc_4380.width,
    #                                    spin = Pc_4380.spin, parity = Pc_4380.parity, aboveLmin = 0,
    #                                    use_BLS_MagAndPhase = True,
    #                                    BSLh_M = {(Pc_4380.name + "__M_Bs3l2hall_Mag") : Const(0.6),
    #                                              (Pc_4380.name + "__M_Bs3l2hall_Phase") : Const(0.2)},
    #                                    BSLh_res = {(Pc_4380.name + "__res_Bs1l2h-1_Mag") : Const(0.4),
    #                                                (Pc_4380.name + "__res_Bs1l2h-1_Phase") : Const(0.),
    #                                                (Pc_4380.name + "__res_Bs1l2h0_Mag")  : Const(0.6),
    #                                                (Pc_4380.name + "__res_Bs1l2h0_Phase")  : Const(0.),
    #                                                (Pc_4380.name + "__res_Bs1l2h1_Mag")  : Const(0.4),
    #                                                (Pc_4380.name + "__res_Bs1l2h1_Phase")  : Const(0.)}
    #                                    ),

    #MatrixElement.AResonanceBreitWigner(name = Pc_4450.name, label = Pc_4450.label,
    #                                    mass = Pc_4450.mass, width = Pc_4450.width,
    #                                    spin = Pc_4450.spin, parity = Pc_4450.parity, aboveLmin = 0,
    #                                    use_BLS_MagAndPhase = True,
    #                                    BSLh_M = {(Pc_4450.name + "__M_Bs5l4hall_Mag") : Const(0.6),
    #                                              (Pc_4450.name + "__M_Bs5l4hall_Phase") : Const(0.2)},
    #                                    BSLh_res = {(Pc_4450.name + "__res_Bs1l4h-1_Mag") : Const(0.4),
    #                                                (Pc_4450.name + "__res_Bs1l4h-1_Phase") : Const(0.),
    #                                                (Pc_4450.name + "__res_Bs1l4h0_Mag")  : Const(0.6),
    #                                                (Pc_4450.name + "__res_Bs1l4h0_Phase")  : Const(0.),
    #                                                (Pc_4450.name + "__res_Bs1l4h1_Mag")  : Const(0.4),
    #                                                (Pc_4450.name + "__res_Bs1l4h1_Phase")  : Const(0.)}
    #                                    ),
]

# list of pi K resonances
B = [
    # Lc --> p Kst890
    MatrixElement.BResonanceBreitWigner(name = Kst890.name, label = Kst890.label,
                                        mass = Kst890.mass, width = Kst890.width,
                                        spin = Kst890.spin, parity = Kst890.parity, aboveLmin = 0,
                                        use_BLS_RealAndImag = True,
                                        couplings_dict_M = {"Kst890__M_Bs1l0hall_Real" : Const(1.),
                                                            "Kst890__M_Bs1l0hall_Imag" : Const(0.)},
                                        couplings_dict_res = {"Kst890__res_Bs0l2h-2_Real" : FitParameter("Kst890__res_Bs0l2h-2_Real", 0.5, -10., 10., 0.01),
                                                              "Kst890__res_Bs0l2h-2_Imag" : FitParameter("Kst890__res_Bs0l2h-2_Imag", 0.5, -10., 10., 0.01),
                                                              "Kst890__res_Bs0l2h0_Real"  : FitParameter("Kst890__res_Bs0l2h0_Real", 0.5, -10., 10., 0.01),
                                                              "Kst890__res_Bs0l2h0_Imag"  : FitParameter("Kst890__res_Bs0l2h0_Imag", 0.5, -10., 10., 0.01),
                                                              "Kst890__res_Bs0l2h2_Real"  : FitParameter("Kst890__res_Bs0l2h2_Real", 0.5, -10., 10., 0.01),
                                                              "Kst890__res_Bs0l2h2_Imag"  : FitParameter("Kst890__res_Bs0l2h2_Imag", 0.5, -10., 10., 0.01)},
                                        resdecay_parity_conservation = True, colour = kBlue, linestyle = 7, fillstyle = 3954),

    # Lb --> Lc Ds*1(2860)
    #MatrixElement.BResonanceBreitWigner(name = Ds1_2860.name, label = Ds1_2860.label,
    #                                    fix_mass = Ds1_2860.mass, fix_width = Ds1_2860.width,
    #                                    spin = Ds1_2860.spin, parity = Ds1_2860.parity, aboveLmin = 0,
    #                                    use_BLS_MagAndPhase = True,
    #                                    fixBLS_Lb_mag = Const(0.2), fixBLS_Lb_phase = Const(0.1),
    #                                    fixBLS_res_toone = True),
]

# list of p K resonances
C = [
    # Lc --> Lambda*(1520) pi
    MatrixElement.CResonanceBreitWigner(name = Lambda1520.name, label = Lambda1520.label,
                                        mass = Lambda1520.mass,
                                        width = Lambda1520.width,
                                       spin = Lambda1520.spin, parity = Lambda1520.parity, aboveLmin = 0,
                                       use_BLS_RealAndImag = True,
                                       couplings_dict_M = {"L1520__M_Bs3l2hall_Real" : Const(1.),
                                                           "L1520__M_Bs3l2hall_Imag" : Const(0.)},
                                       couplings_dict_res = {"L1520__res_Bs1l4h1_Real" : FitParameter("L1520__res_Bs1l4h1_Real", 0.5, -10., 10., 0.01),
                                                             "L1520__res_Bs1l4h1_Imag" : FitParameter("L1520__res_Bs1l4h1_Imag", 0.5, -10., 10., 0.01),
                                                             "L1520__res_Bs1l4h-1_Real" : FitParameter("L1520__res_Bs1l4h-1_Real", 0.5, -10., 10., 0.01),
                                                             "L1520__res_Bs1l4h-1_Imag" : FitParameter("L1520__res_Bs1l4h-1_Imag", 0.5, -10., 10., 0.01)},
                                       resdecay_parity_conservation = True, colour = kPink, linestyle = 7, fillstyle = 3954)

    # Lb --> Xic(2815) D0
    #MatrixElement.CResonanceBreitWigner(name = Xic_2815.name, label = Xic_2815.label,
    #                                    fix_mass = Xic_2815.mass,
    #                                    fix_width = Const(0.04),
    #                                    spin = Xic_2815.spin, parity = Xic_2815.parity, aboveLmin = 0,
    #                                    use_BLS_RealAndImag = True,
    #                                    fixBLS_Lb_real = Const(0.8), fixBLS_Lb_imag = Const(0.2),
    #                                    fixBLS_res_toone = True)

    # Lb --> Xic(3080) D0
    #MatrixElement.CResonanceBreitWigner(name = Xic_3080.name, label = Xic_3080.label,
    #                                    fix_mass = Xic_3080.mass,
    #                                    fix_width = Const(0.08),
    #                                    spin = 3, parity = -1, aboveLmin = 0,
    #                                    use_BLS_MagAndPhase = True,
    #                                    fixBLS_Lb_mag = Const(0.3), fixBLS_Lb_phase = Const(0.1),
    #                                    fixBLS_res_toone = True),
]

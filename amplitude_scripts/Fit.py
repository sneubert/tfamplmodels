
import math
import pypdt
import sys
import argparse
import numpy as np
import importlib
import itertools

import tensorflow as tf
from tensorflow.python import debug as tf_debug

from ROOT import TFile, TH1F, TCanvas

# import the TensorflowAnalysis libraries
import Interface
from Kinematics import *
import Optimisation
import Likelihood
import PDFs

# import the local libraries
import MatrixElement
import PhaseSpace
import Fractions_Interference

# import the Python settings
from python_settings import *

# import some other stuff
from variables_Lc2ppiK import variables

def GetData(branches, filename, treename):

        data_file = TFile.Open(filename)

        return Optimisation.ReadNTuple(data_file.Get(treename), branches)

def reweight(x, phase_space, variables, weight):
	data = []

        for i in range(len(variables)):
		data += [tf.transpose(x)[i]]
	data += [weight]

	return tf.transpose(data)

if __name__ == "__main__" :

        logging.info("")
        logging.info("-------------------------------")
        logging.info("------------- Fit -------------")
        logging.info("-------------------------------")

        ###########
        ## parser
        ###########

        parser = argparse.ArgumentParser(description="Parser of the Fit script")

        # get the input file and tree names of the data sample
        parser.add_argument('--inFile_data_name', dest = 'inFile_data_name',
                            default = 'toy.root', help = "name of the input data sample")
        parser.add_argument('--inTree_data_name', dest = 'inTree_data_name',
                            default = 'toy', help = "tree name of the input data sample")
        parser.add_argument('--weight_data_name', dest = 'weight_data_name',
                            default = '', help = "name of the event weight of the data sample")

        # get the input file and tree names of the normalisation sample
        parser.add_argument('--inFile_norm_name', dest = 'inFile_norm_name',
                            default = 'toy.root', help = "name of the input normalisation sample")
        parser.add_argument('--inTree_norm_name', dest = 'inTree_norm_name',
                            default = 'toy_normalisation', help = "tree name of the input normalisation sample")
        parser.add_argument('--weight_norm_name', dest = 'weight_norm_name',
                            default = '', help = "name of the event weight of the normalisation sample")

        # get the input file where the initial values of the parameters are specified
        parser.add_argument('--in_params', dest='in_params_file_name',
                            default = "", help = "name of the input file containing the initial parameter values")

        # get the input file where the efficiency map is placed, and the histo name
        parser.add_argument('--inFileEff_name', dest='inFileEff_name',
                            default = "", help = "name of the input file containing the efficiency map")
        parser.add_argument('--inHistoEff_name', dest='inHistoEff_name',
                            default = "", help = "name of the efficiency histogram")

	# get the config file name where the resonances to be fitted are described
	parser.add_argument('--config_name', dest = 'config_name',
			    default = 'toy', help = "name of config file where the resonances are described")


        # set the output file name
        parser.add_argument('--outFile_name', dest = 'outFile_name',
                            default='./', help = "output file name")

        # now get the parsed options
        args = parser.parse_args()

        inFile_data_name = args.inFile_data_name
        inTree_data_name = args.inTree_data_name
        weight_data_name = args.weight_data_name

        inFile_norm_name = args.inFile_norm_name
        inTree_norm_name = args.inTree_norm_name
        weight_norm_name = args.weight_norm_name

        in_params_file_name = args.in_params_file_name

        inFileEff_name = args.inFileEff_name
        inHistoEff_name = args.inHistoEff_name

	config_name = args.config_name

        outFile_name = args.outFile_name

        # print the parsed options
        logging.info("\n")
        logging.info("inFile_data_name = {}".format(inFile_data_name))
        logging.info("inTree_data_name = {}".format(inTree_data_name))
        logging.info("weight_data_name = {}".format(weight_data_name))
        logging.info("inFile_norm_name = {}".format(inFile_norm_name))
        logging.info("inTree_norm_name = {}".format(inTree_norm_name))
        logging.info("weight_norm_name = {}".format(weight_norm_name))
        logging.info("in_params_file_name = {}".format(in_params_file_name))
        logging.info("inFileEff_name = {}".format(inFileEff_name))
        logging.info("inHistoEff_name = {}".format(inHistoEff_name))
	logging.info("config_name = {}".format(config_name))
	logging.info("outFile_name = {}".format(outFile_name))

        # initialise the TensorfFlow session
	config = tf.ConfigProto()
        config.gpu_options.allow_growth = True

        sess = tf.Session(config=config)

        logging.debug("puchuu1")
        logging.debug(sess.run(tf.report_uninitialized_variables()))

        # initialise the variables
	init = tf.global_variables_initializer()
        sess.run(init)

        ##########
        ## get the BDT efficiency, and smooth it
        ##########

        eff_shape = None

        # check if I have to take into account the efficiency
        if inFileEff_name != "" :

                # open the input file
                inFileEff = TFile.Open(inFileEff_name)

                # check that the file exists
                if not inFileEff :
                        logging.error("Error: the efficiency file does not exist.\n")
                        sys.exit(1)

                # get the eff histo
                effHisto = inFileEff.Get(inHistoEff_name)

                # check that the histo exists
                if not effHisto :
                        logging.error("Error: the efficiency histo does not exist.\n")
                        sys.exit(1)

                # close the input file
                effHisto.SetDirectory(0)
                inFileEff.Close()

                #smooth the efficiency histo, and get a continuos shape
                effHisto.Smooth()
                eff_shape = PDFs.RootHistShape(effHisto)

                logging.info("Efficiency histo succesfully smoothed and shaped.\n")

        ############

        # set the a few options for the MatrixElement mondule
        MatrixElement.sess = sess

        PhaseSpace.sess = sess

        nprojects = 100000

	phase_space = PhaseSpace.ThreeBodyPhaseSpace()

        # import the resonances from the config file
        sys.path.append(os.path.dirname(config_name))
        resonance_module = importlib.import_module(os.path.basename(config_name)[:-3])  #-3 removes the '.py' at the end of the string

        non_res = resonance_module.non_res
        A = resonance_module.A
        B = resonance_module.B
        C = resonance_module.C

        resonances = A + B + C

        # let's reinitialise the variables
        init = tf.global_variables_initializer()
        sess.run(init)

        # print the B_LS couplings of the resonances, for debug
        for res in resonances:
                MatrixElement.print_BSL(res.BSL_M, res.name + "__M")
                MatrixElement.print_BSL(res.BSL_res, res.name + "__res")

	logging.debug(sess.run(tf.report_uninitialized_variables()))

        logging.info("")
        logging.info("Imported resonances:")
        logging.info("non_res = {}".format(non_res))
        logging.info("A = {}".format([A_res.name for A_res in A]))
        logging.info("B = {}".format([B_res.name for B_res in B]))
        logging.info("C = {}\n".format([C_res.name for C_res in C]))

        # list of switches (flags that control the components of the PDF)
        # +1 stands for the non-resonant contribution
        switches = Optimisation.Switches(len(resonances) + 1)

        def event_weight(datapoint, normalisation = 1.):
		return tf.transpose(datapoint)[-1] * normalisation  # dangerous: assume the weight
                                                                    # is the last element of the list

        def MC_kin_w(datapoint):
		theta = phase_space.PhiChicJpsi(datapoint)*event_weight(datapoint)
		return 8./(3.*(3. - Cos(theta)**2))

        def model(datapoint, component_index = -1, eff_shape = None):
		return MatrixElement.MatrixElement(phase_space, datapoint, switches,
                                                   component_index, resonances, non_res, eff_shape)

        # build the PDFs
        data_placeholder = phase_space.data_placeholder
	norm_placeholder = phase_space.norm_placeholder

	data_pdf = model(data_placeholder)
	norm_pdf = model(norm_placeholder)

        # set the initial parameter values from an external file
        if in_params_file_name != "" :
                new_values = Optimisation.ReadParameters(in_params_file_name)
                Optimisation.SetParameters(new_values)

        # print the parameter values
        Optimisation.PrintParameters()

        logging.info("Loading the data from: {}".format(inFile_data_name))

        # get the data, and the sweights
        unfiltered_data_sample = GetData([var[1] for var in variables] + [weight_data_name],
                                         inFile_data_name, inTree_data_name)

        # filter the data, checking for the allowed phase space
        data_sample = sess.run(phase_space.Filter(unfiltered_data_sample))

        logging.info("Loading the normalisation sample from: {}".format(inFile_norm_name))

        # get the normalisation sample, and the weights
        unfiltered_norm_sample = GetData([var[1] for var in variables] + [weight_norm_name],
                                         inFile_norm_name, inTree_norm_name)

        # filter the normalisation sample, checking for the allowed phase space
        norm_sample = sess.run(phase_space.Filter(unfiltered_norm_sample))

        # reweight the normalisation sample, if needed
        #print "Reweighting simulation..."
        #norm_sample = sess.run(reweight(norm_placeholder, phase_space, variables, MC_kin_w(norm_placeholder)), {norm_placeholder: norm_sample})

        ##########
        ## perform the fit to the data
        ##########

        integral = Likelihood.Integral(norm_pdf)
        #integral = Likelihood.WeightedIntegral(norm_pdf, event_weight(norm_placeholder)*MC_kin_w(norm_placeholder))

        # normalisation factor to propagate properly the errors
        # the s-weight is the last element of the data points
        sweights_normalisation = sum([dp[-1] for dp in data_sample])/sum([dp[-1]**2 for dp in data_sample])

        # get the unbinned weighted negative log likelihood for the PDF
        #nll = Likelihood.UnbinnedWeightedNLL(data_pdf, integral,
        #                                     event_weight(data_placeholder,                          # get the s-weight of the event,
        #                                                  normalisation = sweights_normalisation))   # multiplied by a normalisation factor
        #                                                                                             # to propagate properly the errors

        # print the B_LS couplings of the resonances, for debug
        for res in resonances:
                MatrixElement.print_BSL(res.BSL_M, res.name + "__M")
	        MatrixElement.print_BSL(res.BSL_res, res.name + "__res")

        # get the negative log of the unbinned weighted --> extendend <-- likelihood
        nll = Likelihood.UnbinnedWeightedExtendedNLL(data_pdf, integral,
                                                     event_weight(data_placeholder),      # get the s-weight of the event,
                                                     sweights_normalisation)              # multiplied by a normalisation factor
                                                                                          # to propagate properly the errors

        # perform the fit
        result = Optimisation.RunMinuit(sess, nll, {data_placeholder: data_sample, norm_placeholder: norm_sample},
                                        tmpFile = outFile_name + "_tmp_results.txt",
                                        useGradient = True)

        # write out the fit results
        Optimisation.WriteFitResults(result, outFile_name + "_results.info")

        ##########
        ## compute and write out the fit fractions and the interference terms
        ##########

        # total PDF integral over the normalisation sample
        #pdf_norm = sess.run(norm_pdf, feed_dict = {norm_placeholder : norm_sample} )
        #total_int = np.sum(pdf_norm)

        fit_fractions, total_int = Fractions_Interference.write_fit_fractions(sess, data_pdf, data_placeholder,
                                                                              switches, norm_sample,
                                                                              resonances, outFile_name)

        # compute the interference terms only if you have more then one resonance
        if len(resonances) > 1 :
                Fractions_Interference.write_interference_terms(sess, phase_space, total_int, norm_sample,
                                                                resonances, outFile_name)

        #############
        ## Save out the B_LS couplings
        #############

	# print the B_LS couplings of the resonances, for debug
        for res in resonances:
	        MatrixElement.print_BSL(res.BSL_M, res.name + "__M")
		MatrixElement.print_BSL(res.BSL_res, res.name + "__res")

	# open the output files with the parameters
        outFile_params_cartesian = open(outFile_name + "_params_cartesian.info", "w")
        outFile_params_polar = open(outFile_name + "_params_polar.info", "w")

        # write out the values of the B_LS couplings
	# and the size of the non-resonant contribution
        # in both cartesian and polar coordinates
        [MatrixElement.SaveBSL_cartesian(res, outFile_params_cartesian) for res in resonances]
	non_res.SaveNonResSize(outFile_params_cartesian)

        ###########

        # close the session
        sess.close()

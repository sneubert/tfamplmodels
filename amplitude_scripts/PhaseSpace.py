
import tensorflow as tf
import pypdt
import sys
import numpy as np
import math

#import the local libraries
import Interface
from Interface import fptype, Cos, Acos, Const, Atan2

import Kinematics

from Constants import *

# import the logging configuration
import logging
#from logging.config import fileConfig
#
#logging.config.fileConfig('../include/logging.ini')

######

sess = None

######

class ThreeBodyPhaseSpace :
	"""
	Class for 3-body decay phase space M -> d1 d2 d3 expressed as:
	  cosThetaM   : cos(thetaM), cosine of the helicity angle of M
	  cosThetaA   : cos(thetaA), cosine of the helicity angle of A
          phid1       : angle between the M and A planes in the M rest frame
        Remember that phid3 == 0 by convention!
	"""

        def __init__(self) :
                """
                  Constructor                                                                                                       
                """

                # set the masses
		self.m_M = M.mass
		self.m_d1 = d1.mass
		self.m_d2 = d2.mass
		self.m_d3 = d3.mass

                self.m_A_min = self.m_d1 + self.m_d2
		self.m_A_max = self.m_M - self.m_d3
                                
                self.m_B_min = self.m_d2 + self.m_d3
                self.m_B_max = self.m_M - self.m_d1
                
                self.m_C_min = self.m_d1 + self.m_d3
                self.m_C_max = self.m_M - self.m_d2
                                
		self.data_placeholder = self.Placeholder("data")
		self.norm_placeholder = self.Placeholder("data")

        def Inside(self, x) :
                """
                  Check if the point x is inside the phase space
                """
                
                m_d1d2 = self.m_d1d2(x)
                m_d2d3 = self.m_d2d3(x)
                m_d1d3 = self.m_d1d3(x)
                
                # helicity angles
       		cthLb = self.cosThetaM(x)
		cthA = self.cosThetaA(x)
		
                # phid1: phi angle between the M and A planes in the M rest frame 
                phid1 = self.phid1(x)
                
                # remember that phid3 == 0!
                
                # check if the cosin and phi angles are in the phase space
                inside = tf.logical_and(tf.logical_and(tf.logical_and(tf.greater(cthLb, -1.), tf.less(cthLb, 1.)), \
		                                       tf.logical_and(tf.greater(cthA, -1.), tf.less(cthA, 1.))), \
                                        tf.logical_and(tf.greater(phid1, -3.14), tf.less(phid1, 3.14)))
                
                # check if the invariant masses are in the phase space
                inside = tf.logical_and(inside,
                                        tf.logical_and(tf.logical_and(tf.greater(m_d1d2, self.m_A_min), tf.less(m_d1d2, self.m_A_max)),
                                                       tf.logical_and(tf.logical_and(tf.greater(m_d2d3, self.m_B_min), tf.less(m_d2d3, self.m_B_max)),
                                                                      tf.logical_and(tf.greater(m_d1d3, self.m_C_min), tf.less(m_d1d3, self.m_C_max)))))
                
		return inside
        
	def Filter(self, x) :
                """
                  Filter a sample, checking that it is inside the allowed phase space
                """
                
		return tf.boolean_mask(x, self.Inside(x) )

        def Density(self, x) :
                """
                  Get the density, playing with tensors
                """
	        
		m_A = self.m_d1d2(x)

                #ones = Ones(m_A)        # I would need it only in case of some more dn contributions without m_A inside, like:
                #                        d3 = TwoBodyMomentum(self.m_d2, self.m_pi, self.m_d3*ones)

                d1 = Kinematics.TwoBodyMomentum(self.m_M, m_A, self.m_d3)    # M --> A d3
		d2 = Kinematics.TwoBodyMomentum(m_A, self.m_d2, self.m_d1)   # A --> d1 d2
		
		return d1*d2/self.m_M

        def Density_NotTensor(self, x) :
                """
                  Get the density
                """

                # here the A mass is floating!
                
		d1 = Kinematics.TwoBodyMomentum_NotTensor(self.m_M, x, self.m_d3)    # M --> A d3
		d2 = Kinematics.TwoBodyMomentum_NotTensor(x, self.m_d2, self.m_d1)   # A --> d1 d2
                
                return d1*d2/self.m_M

	def m_d1d2Sample(self, size) :

                """
                  Generate a sample uniform in the d1d2 mass distribution
                """
                
		sample = []

                ymin = 0.
		ymax = self.Density_NotTensor(np.random.uniform(self.m_A_min, self.m_A_max, 10000)).max()

                while len(sample) < size:
			x = np.random.uniform(self.m_A_min, self.m_A_max)
			y = np.random.uniform(ymin, ymax)
                                        
			if y < self.Density_NotTensor(x):
				sample.append(x)

		return np.array(sample).astype('d')

        def UnfilteredSample(self, size, majorant = -1) :
	        """
	          Generate a uniform sample of point within phase space.
	          size     : number of _initial_ points to generate. Not all of them will fall into phase space,
	                     so the number of points in the output will be < size.
	          majorant : if majorant>0, add another dimension to the generated tensor which is
	                     uniform number from 0 to majorant. Useful for accept-reject toy MC.
                """
        
	        v = [
                        # m_d1d2
			self.m_d1d2Sample(size),
                        
                        # cos(thetaM_A)
                        np.random.uniform(-1., 1., size ).astype('d'),

                        # cos(thetaA)
                        np.random.uniform(-1., 1., size ).astype('d'),

                        # phid1_A
                        np.random.uniform(-math.pi, math.pi, size ).astype('d'),
                ]

                if majorant > 0 :
                        v += [ np.random.uniform( 0., majorant, size).astype('d') ]

		return np.transpose(np.array(v))

	def UniformSample(self, size, majorant = -1) :
		"""
		  Generate a uniform sample of point within phase space.
		    size     : number of _initial_ points to generate. Not all of them will fall into phase space,
		               so the number of points in the output will be <size.
		    majorant : if majorant>0, add another dimension to the generated tensor which is
		               uniform number from 0 to majorant. Useful for accept-reject toy MC.
		  Note it does not actually generate the sample, but returns the data flow graph for generation,
		  which has to be run within TF session.
		"""
		return self.Filter( self.UnfilteredSample(size, majorant) )

        ##########
        
        # basic variables to describe the amplitudes

        def m_d1d2(self, sample) :
        	return tf.transpose(sample)[0]
        
        def cosThetaM(self, sample) :
                return tf.transpose(sample)[1]
                                        
	def cosThetaA(self, sample) :
		return tf.transpose(sample)[2]

	def phid1(self, sample) :
		return tf.transpose(sample)[3]

        ##########

        # some other interesting variables:
        # invariant masses of the subsystems, other angles
        
        def m_d2d3(self, sample) :
                """
                  Compute the invariant mass of the d2d3 subsystem
                """
                
                qvect_d3, qvect_d1, qvect_d2 = self.FinalStateMomenta(sample)

                return Kinematics.Mass(Kinematics.SumFourVectors([qvect_d2, qvect_d3]))

        def m_d1d3(self, sample) :
                """
                  Compute the invariant mass of the d1d3 subsystem
                """

                qvect_d3, qvect_d1, qvect_d2 = self.FinalStateMomenta(sample)
                
                return Kinematics.Mass(Kinematics.SumFourVectors([qvect_d1, qvect_d3]))

        def cosThetaB(self, sample) :
                """
                  Compute cos(thetaB)
                """
                
                theta_B = self.BCoordinates(sample)["theta_B"]
                
                return Cos(theta_B)

        def cosThetaM_B(self, sample) :
                
                theta_M_B = self.BCoordinates(sample)["theta_M_B"]

                return Cos(theta_M_B)
        
        def m2_d2K(self, sample) :
                
                m2_d2K, theta_M_B, theta_B, phi_d1_B, phi_d2_B, theta_d1_AB = self.BCoordinates(sample)

                return m2_d2K
        
        def phid1_B(self, sample) :
                """
                  Compute phi_d1 in the B coordinate system
                """
                
                phi_d1_B = self.BCoordinates(sample)["phi_d1_B"]
                
                return phi_d1_B

        def phid2_B(self, sample) :

                phi_d2_B = self.BCoordinates(sample)["phi_d2_B"]

                return phi_d2_B
        
        def cosThetaC(self, sample) :
                """
                  Compute cos(thetaC)
                """
                
                theta_C = self.CCoordinates(sample)["theta_C"]
                        
                return Cos(theta_C)

        def cosThetaM_C(self, sample) :
                
                theta_M_C = self.CCoordinates(sample)["theta_M_C"]

                return Cos(theta_M_C)

        def phid2_C(self, sample) :
                """
                  Compute phi_d2 in the C coordinate system
                """
                
                phi_d2_C = self.CCoordinates(sample)["phi_d2_C"]

                return phi_d2_C

        def phid1_C(self, sample) :
                """
                  Compute phi_d1 in the C coordinate system
                """
                
                phi_d1_C = self.CCoordinates(sample)["phi_d1_C"]

                return phi_d1_C
        
        ##########

        # some auxiliary functions
        # to compute the 4-moments of the particles
        # and the kinematic variables
        
        def M_qvect(self, sample) :
                """
                  Get the Lb 4-momenta
                """
                
                qvect_d3, qvect_d1, qvect_d2 = self.FinalStateMomenta(sample)

                #compute the Lb 4-momenta
                return Kinematics.SumFourVectors([qvect_d1, qvect_d2, qvect_d3])

        def d1_qvect(self, sample) :
                """
                  Get the d1 4-momenta 
                """

                qvect_d3, qvect_d1, qvect_d2 = self.FinalStateMomenta(sample)

                return qvect_d1

        def d2_qvect(self, sample) :
                """
                  Get the d2 4-momenta
                """

                qvect_d3, qvect_d1, qvect_d2 = self.FinalStateMomenta(sample)

                return qvect_d2

        def d3_qvect(self, sample) :
                """  
                  Get the d3 4-momenta
                """

                qvect_d3, qvect_d1, qvect_d2 = self.FinalStateMomenta(sample)

                return qvect_d3

        def M_PX(self, sample) :
                return Kinematics.XComponent(self.M_qvect(sample))

        def M_PY(self, sample) :
                return Kinematics.YComponent(self.M_qvect(sample))

        def M_PZ(self, sample) :
                return Kinematics.ZComponent(self.M_qvect(sample))

        def M_E(self, sample) :
                return Kinematics.TimeComponent(self.M_qvect(sample))

        def d1_PX(self, sample) :
                return Kinematics.XComponent(self.d1_qvect(sample))

        def d1_PY(self, sample) :
                return Kinematics.YComponent(self.d1_qvect(sample))

        def d1_PZ(self, sample) :
                return Kinematics.ZComponent(self.d1_qvect(sample))

        def d1_E(self, sample) :
                return Kinematics.TimeComponent(self.d1_qvect(sample))

        def d2_PX(self, sample) :
                return Kinematics.XComponent(self.d2_qvect(sample))

        def d2_PY(self, sample) :
                return Kinematics.YComponent(self.d2_qvect(sample))

        def d2_PZ(self, sample) :
                return Kinematics.ZComponent(self.d2_qvect(sample))

        def d2_E(self, sample) :
                return Kinematics.TimeComponent(self.d2_qvect(sample))

        def d3_PX(self, sample) :
                return Kinematics.XComponent(self.d3_qvect(sample))

        def d3_PY(self, sample) :
                return Kinematics.YComponent(self.d3_qvect(sample))

        def d3_PZ(self, sample) :
                return Kinematics.ZComponent(self.d3_qvect(sample))

        def d3_E(self, sample) :
                return Kinematics.TimeComponent(self.d3_qvect(sample))

        ##########
        
	def FinalStateMomenta(self, x, test = False) :
		"""
		  Returns the final state momenta for the decay defined by the phase space vector x.
		  The momenta are calculated in the Lambda_b frame.
		"""
                      
                # if in test-mode, overwrite the "x" dataset with a toy event
                # and print a set of infos at the end of the transformations
                if test :
                        x = self.UnfilteredSample(2)
                        
                ones = Interface.Ones(self.m_d1d2(x))
		zeros = Interface.Zeros(self.m_d1d2(x))    #only used to set phid3_A == 0 
                
                # get the A and d3 four-momenta in the Lb rest frame from the angles
                qvect_d3_Lbrest, qvect_A_Lbrest = Kinematics.FourMomentaFromHelicityAngles(ones*self.m_M, ones*self.m_d3, self.m_d1d2(x),
                                                                                    Acos(self.cosThetaM(x)), zeros)

                # get the d1 and d2 four-momenta in the A rest frame from the angles
                qvect_d2_Arest, qvect_d1_Arest = Kinematics.FourMomentaFromHelicityAngles(self.m_d1d2(x), ones*self.m_d2, ones*self.m_d1,
                                                                                    Acos(self.cosThetaA(x)), self.phid1(x))
                
                # boost the d1 and d2 four-momenta to the Lb rest frame,
                # boosting by -qvect_A (A is decaying to d1 and d2!)
                # Pay attention: this method is BoostAndRotation,
                # instead in the next RotationAndBoost will be used
                qvect_d1_Lbrest, qvect_d2_Lbrest = Kinematics.BoostAndRotation([qvect_d1_Arest, qvect_d2_Arest], qvect_A_Lbrest)

                # print some informations for the test-mode
                if test :
                        logging.info("TEST x = {}".format(x))
                        
                        logging.info("FinalStateMomenta::qvect_d3_Lbrest = {}".format(sess.run(qvect_d3_Lbrest)))
                        logging.info("FinalStateMomenta::qvect_A_Lbrest = {}".format(sess.run(qvect_A_Lbrest)))
                        
                        logging.info("FinalStateMomenta::qvect_d2_Arest = {}".format(sess.run(qvect_d2_Arest)))
                        logging.info("FinalStateMomenta::qvect_d1_Arest = {}".format(sess.run(qvect_d1_Arest)))
                        
                        logging.info("FinalStateMomenta::qvect_d2_Lbrest = {}".format(sess.run(qvect_d2_Lbrest)))
		        logging.info("FinalStateMomenta::qvect_d1_Lbrest = {}".format(sess.run(qvect_d1_Lbrest)))
                        
		        logging.info("FinalStateMomenta::T sum d2, d1 in A rest frame = {}".format(sess.run(Kinematics.TimeComponent(qvect_d2_Arest)
                                                                                                            + Kinematics.TimeComponent(qvect_d1_Arest))))
                        logging.info("FinalStateMomenta::M of A in Lb rest frame = {}".format(sess.run(Kinematics.Mass(qvect_A_Lbrest))))
                        
                        logging.info("FinalStateMomenta::X sum d2, d1, d3 in Lb rest frame = {}".format(sess.run(Kinematics.XComponent(qvect_d1_Lbrest)
                                                                                                                + Kinematics.XComponent(qvect_d2_Lbrest)
                                                                                                                + Kinematics.XComponent(qvect_d3_Lbrest))))
                        logging.info("FinalStateMomenta::Y sum d2, d1, d3 in Lb rest frame = {}".format(sess.run(Kinematics.YComponent(qvect_d1_Lbrest)
                                                                                                                + Kinematics.YComponent(qvect_d2_Lbrest)
                                                                                                                + Kinematics.YComponent(qvect_d3_Lbrest))))
                        logging.info("FinalStateMomenta::Z sum d2, d1, d3 in Lb rest frame = {}".format(sess.run(Kinematics.ZComponent(qvect_d1_Lbrest)
                                                                                                                + Kinematics.ZComponent(qvect_d2_Lbrest)
                                                                                                                + Kinematics.ZComponent(qvect_d3_Lbrest))))
                        logging.info("FinalStateMomenta::T sum d2, d1, d3 in Lb rest frame = {}".format(sess.run(Kinematics.TimeComponent(qvect_d1_Lbrest)
                                                                                                                + Kinematics.TimeComponent(qvect_d2_Lbrest)
                                                                                                                + Kinematics.TimeComponent(qvect_d3_Lbrest))))
                
	        return qvect_d3_Lbrest, qvect_d1_Lbrest, qvect_d2_Lbrest

        def ACoordinates_fromPcomponents(self, sample):

                d1_px = tf.transpose(sample)[0]
                d1_py = tf.transpose(sample)[1]
                d1_pz = tf.transpose(sample)[2]
                d1_E  = tf.transpose(sample)[3]
                
                d2_px = tf.transpose(sample)[4]
                d2_py = tf.transpose(sample)[5]
                d2_pz = tf.transpose(sample)[6]
                d2_E  = tf.transpose(sample)[7]

                d3_px = tf.transpose(sample)[8]
                d3_py = tf.transpose(sample)[9]
                d3_pz = tf.transpose(sample)[10]
                d3_E  = tf.transpose(sample)[11]

                # build the 4-momenta
                qvect_d1 = Kinematics.LorentzVector(Kinematics.Vector(d1_px, d1_py, d1_pz), d1_E)
                qvect_d2 = Kinematics.LorentzVector(Kinematics.Vector(d2_px, d2_py, d2_pz), d2_E)
                qvect_d3 = Kinematics.LorentzVector(Kinematics.Vector(d3_px, d3_py, d3_pz), d3_E)
                
                logging.debug("PhaseSpace::ACoordinates_fromPcomponents: qvect_d1 = {}".format(sess.run(qvect_d1)))
                logging.debug("PhaseSpace::ACoordinates_fromPcomponents: qvect_d2 = {}".format(sess.run(qvect_d2)))
                logging.debug("PhaseSpace::ACoordinates_fromPcomponents: qvect_d3 = {}".format(sess.run(qvect_d3)))
                
                #########
                # m2 of d1d2, in GeV^2/c^4
                #########

                qvect_A = Kinematics.SumFourVectors([qvect_d1, qvect_d2])
                m2_d1d2 = (Kinematics.Mass(qvect_A))**2
                
                ##########
                # thetaM, in the M rest frame
                ##########
                
                # fill the M 4-momenta
                qvect_Lb = qvect_A + qvect_d3
                
                # boost the d3 to the M rest frame
                qvect_d3_Lbframe = Kinematics.RotationAndBoost([qvect_d3], qvect_Lb)[0]

                # calculate the angles:
	        # polar (theta) and azimuthal (phi) angles of the d3 three-vector in the Lb rest frame
                theta_M_A, phi_d3_A = Kinematics.HelicityAngles(qvect_d3_Lbframe)

                # set the phi_d3_A angle = 0. by definition
                # well, I'm not returning it anyway...
                #phi_d3_A = Interface.Zeros(sample)
                
                #########
                # thetaA, in the A rest frame
                ##########

                # rotate and boost the d1 momenta to the rest frame of A
                qvect_d1_Aframe = Kinematics.RotationAndBoost([qvect_d1], qvect_A)[0]

                # calculate the angles:
                # polar (theta) and azimuthal (phi) angles of the d1 three-vector in the A rest frame
                theta_A, phi_d1_A = Kinematics.HelicityAngles(qvect_d1_Aframe)

                ####

                # before that we leave, let's print the qvects in the Lb rest frame
                # just for debug, if needed
                qvect_d1_Lbframe, qvect_d2_Lbframe = Kinematics.RotationAndBoost([qvect_d1, qvect_d2], qvect_Lb)
                
                logging.debug("PhaseSpace::ACoordinates_fromPcomponents: qvect_d1_Lbframe = {}".format(sess.run(qvect_d1_Lbframe)))
                logging.debug("PhaseSpace::ACoordinates_fromPcomponents: qvect_d2_Lbframe = {}".format(sess.run(qvect_d2_Lbframe)))
                logging.debug("PhaseSpace::ACoordinates_fromPcomponents: qvect_d3_Lbframe = {}".format(sess.run(qvect_d3_Lbframe)))
                
                return m2_d1d2, theta_M_A, theta_A, phi_d1_A
                
        def ACoordinates(self, x):
                """
                  Returns the invariant mass and angles needed for the A decay chain,
                  as a dictionary
                """

                m2_d1d2 = self.m_d1d2(x)**2
                theta_M_A = Acos(self.cosThetaM(x))
		theta_A = Acos(self.cosThetaA(x))
                phi_d1_A = self.phid1(x)

                # build the dictionary
                dictionary = {"m2_d1d2"    : self.m_d1d2(x)**2,
                              "theta_M_A" : Acos(self.cosThetaM(x)),
                              "theta_A"    : Acos(self.cosThetaA(x)),
                              "phi_d1_A"   : self.phid1(x),
                }
                
                return dictionary
        
	def BCoordinates(self, x, test = False):
		"""
		  Returns the invariant mass and angles needed for the B decay chain,
                  as a dictionary
                """
                
                # compute the four-momenta, in the Lb decay frame
                qvect_d3, qvect_d1, qvect_d2 = self.FinalStateMomenta(x, test)
                
                # calculate the angles:
                # polar (theta) and azimuthal (phi) angles of the d1 three-vector in the Lb frame
                theta_M_B, phi_d1_B = Kinematics.HelicityAngles(qvect_d1)
                
                qvect_B = Kinematics.SumFourVectors([qvect_d2, qvect_d3])
                
                # rotate and boost the d2 momenta to the rest frame of the B
                qvect_d2_B = Kinematics.RotationAndBoost([qvect_d2], qvect_B)[0]  # it takes as input a list of 4-momenta to transform,
                                                                                  # it returns the list of transformed 4-momenta

                #qvect_d2_B = Kinematics.BoostAndRotation([qvect_d2], qvect_B)[0]
                
                # calculate the angles:
                # polar (theta) and azimuthal (phi) angles of the d2 three-vector in the B rest frame
                theta_B, phi_d2_B = Kinematics.HelicityAngles(qvect_d2_B)
                
                #########
                # now I have to compute alpha_d1_AB and theta_d1_AB,
                # because of the different d1 helicity axes in the A and B decay chains
                #########        
                
                # boost the d2 and B to the d1 rest frame
                qvect_d2_d1Rest, qvect_B_d1Rest = Kinematics.RotationAndBoost([qvect_d2, qvect_B], qvect_d1)
                
                # get the three-momenta versors
                p3vers_d2 = Kinematics.UnitVector(Kinematics.SpatialComponents(qvect_d2_d1Rest))
                p3vers_B = Kinematics.UnitVector(Kinematics.SpatialComponents(qvect_B_d1Rest))
                
                # compute theta_d1_AB
                theta_d1_AB = Acos(Kinematics.ScalarProduct(p3vers_d2, p3vers_B))
                
                #####
                
                # aplha_d1_AB is == 0. by 'construction'

                #####
                
                # build the dictionary
		dictionary = {"m2_d2d3"     : Kinematics.Mass(Kinematics.SumFourVectors([qvect_d2, qvect_d3]))**2,
                              "theta_M_B"   : theta_M_B,
                              "theta_B"     : theta_B,
                              "phi_d1_B"    : phi_d1_B,
                              "phi_d2_B"    : phi_d2_B,
                              "theta_d1_AB" : theta_d1_AB
                }
                
		# return the coordinates
		return dictionary

        def CCoordinates(self, x, test = False):
                """
                  Returns the invariant mass and angles needed for the C decay chain,
                  as a dictionary
                """
                
                # compute the four-momenta, in the Lb decay frame
                qvect_d3, qvect_d1, qvect_d2 = self.FinalStateMomenta(x, test)
                
                # calculate the angles
                # polar (theta) and azimuthal (phi) angles of the d2 three-vector in the Lb frame
                theta_M_C, phi_d2_C = Kinematics.HelicityAngles(qvect_d2)

                qvect_C = Kinematics.SumFourVectors([qvect_d1, qvect_d3])
                
                # rotate and boost the C momenta to the rest frame of the d2
                #qvect_C_d2 = Kinematics.RotationAndBoost([qvect_C], qvect_d2)[0]  # Probably have to rotate by some phi angle
                #
                # calculate the angles
                #theta_C, phi_d1_C = Kinematics.HelicityAngles(qvect_C_d2)
                
                # rotate and boost the d1 momenta to the rest frame of C
                qvect_d1_C = Kinematics.RotationAndBoost([qvect_d1], qvect_C)[0]  # it takes as input a list of 4-momenta to transform,
                      	      	      	      	      	      	      	 # it returns the list of transformed 4-momenta
                                                                         
                theta_C, phi_d1_C = Kinematics.HelicityAngles(qvect_d1_C)
                
                #########
                # now I have to compute alpha_d1_AB and theta_d1_AB,
                # because of the different d1 helicity axes in the A and B decay chains
                #########

                """
                theta_d1_AC = Const(0.)
                return (Kinematics.Mass(Kinematics.SumFourVectors([qvect_d1, qvect_d3]))**2,
                        theta_M_C, theta_C, phi_d2_C, phi_d1_C, theta_d1_AC)
                """
        
                # boost the d2 and C to the d1 rest frame
                qvect_d2_d1Rest, qvect_C_d1Rest = Kinematics.RotationAndBoost([qvect_d2, qvect_C], qvect_d1)
                
                # get the three-momenta versors
                p3vers_d2 = Kinematics.UnitVector(Kinematics.SpatialComponents(qvect_d2_d1Rest))
                p3vers_C = Kinematics.UnitVector(Kinematics.SpatialComponents(qvect_C_d1Rest))
                
                # compute theta_d1_AC
                theta_d1_AC = Acos(Kinematics.ScalarProduct(p3vers_d2, p3vers_C))
                
                #####
                
                # aplha_d1_AC is == 0. by 'construction'

                #####

                # build the dictionary
	        dictionary = {"m2_d1d3"     : Kinematics.Mass(Kinematics.SumFourVectors([qvect_d1, qvect_d3]))**2,
                              "theta_M_C"   : theta_M_C,
                              "theta_C"     : theta_C,
                              "phi_d2_C"    : phi_d2_C,
                              "phi_d1_C"    : phi_d1_C,
                              "theta_d1_AC" : theta_d1_AC
                }
                
                # return the coordinates
                return dictionary
                
	def Placeholder(self, name = None) :
		return tf.placeholder(fptype, shape = (None, None), name = name )



#import global_vars

import math
import MatrixElement
import PhaseSpace
import pypdt
import sys
import os
import argparse
import numpy as np
import importlib

import tensorflow as tf
from tensorflow.python import debug as tf_debug

from ROOT import TFile, TH1F, TCanvas, TPad, TH1D, TLegend, TLine, gStyle

from variables_Lc2ppiK import variables, other_variables

# import the Tensorflow stuff
from Interface import *
from Kinematics import *
import Optimisation
import ToyMC

from Constants import *

# import the Python settings
from python_settings import *

def GetData(branches, filename, treename):
	data_file = TFile.Open(filename)

        return ReadNTuple(data_file.Get(treename), branches)

def StyleAxis(axis):
        axis.SetTitleFont(132)
        axis.SetLabelFont(132)
        axis.SetNdivisions(505)
        axis.SetTitleSize(0.065)
        axis.SetTitleOffset(0.80)
        axis.SetLabelSize(0.055)
        axis.SetLabelOffset(0.015)

def ScaleAxisFonts(axis, scale):
        axis.SetTitleSize(axis.GetTitleSize()*scale)
        axis.SetTitleOffset(axis.GetTitleOffset()/scale)
        axis.SetLabelSize(axis.GetLabelSize()*scale)
        axis.SetLabelOffset(axis.GetLabelOffset()/scale)

def SetXTitle(hist, title, unit = ""):
        title = "#font[132]{}"+title
        if len(unit) > 0:
                title += " #font[132]{}[" + unit + "]"
        hist.GetXaxis().SetTitle(title)

def SetYTitle(hist, unit = ""):
        binw = hist.GetXaxis().GetBinWidth(1)
        if binw > 10:
                binw = int(round(binw))
        else:
                binw = round(binw, int(-math.floor(math.log10(binw))))
        title = "#font[132]{}Candidates / (" + str(binw)
        if len(unit) > 0:
                title += " " + unit
        title += ")"
        hist.GetYaxis().SetTitle(title)

def StyleHist(hist, title, unit = ""):
        StyleAxis(hist.GetXaxis())
        StyleAxis(hist.GetYaxis())
        SetXTitle(hist, title, unit)
        SetYTitle(hist, unit)

def StyleCanvas(canv):
        canv.SetMargin(0.11,0.05,0.15,0.05)
        canv.SetTicks(1,1)

def AddVariables_ToSample(x, phsp, variables, weighted = False):

        """""
          Add the required variables to the sample
            x         : datapoint
            phsp      : phase space
            variables : variables to add
            weighted  : to add the weight to the sample, assuming is the last element of the datapoint
        """""

        data = []

        for var in variables:
                data += [eval("phsp." + var[0] + "(x)")]

        # is the weight included in the dataset already?
        # Otherwise, add 1. as weight
        if weighted:
                data += [tf.transpose(x)[-1]]
        else:
                data += [Ones(tf.transpose(x)[-1])]

        return tf.transpose(data)

def PullPlot(exphist, obshist):

        """""
          Plot the pulls
            exphist : histogram with expected data (i.e., the fit)
            obshist : histogram with observed data (i.e., the experimental data)
        """""

        # get a clone of the observed histo
        pullhist = obshist.Clone(obshist.GetName() + "pull")

        # subtract the expected data from the observed
        if(pullhist.Add(exphist, -1)):

                # loop over the bins
                for i in range(pullhist.GetNbinsX()):

                        # bin content = (observed - expected)/sqrt( d(observed)^2 + d(expected)^2 )
                        bincontent = pullhist.GetBinContent(i+1)

                        denominator = math.sqrt(obshist.GetBinError(i+1)**2 + exphist.GetBinError(i+1)**2)

                        if denominator == 0 :
                                logging.info("PullPlot:: Warning: (obs - exp) = {}, denominator = {}".format(bincontent, denominator))
                                denominator = 1.

                        bincontent /= denominator

                        pullhist.SetBinContent(i+1, bincontent)
                        pullhist.SetBinError(i+1, 0)

        # set some style
        pullhist.SetLineColor(kAzure + 3)
        pullhist.SetMarkerColor(kAzure + 3)
        pullhist.SetFillColor(kAzure + 3)
        pullhist.SetDrawOption("e1p")

        #newmax = max(pullhist.GetMaximum(), -pullhist.GetMinimum(), 4)
        #
        #pullhist.SetMaximum(newmax)
        #pullhist.SetMinimum(-newmax)

        pullhist.SetMaximum(6.)
        pullhist.SetMinimum(-6.)

        return pullhist

def getname(index, resonances):

        if index < len(resonances):
                return resonances[index].name
        else:
                return "non-resonant"

def StyleComponentHist(hist, index, resonances):

        # default values for the non-resonant contribution
        name = "non-resonant"
        linewidth = 2
        colour = 30
        linestyle = 9
        fillstyle = 0

        if index < len(resonances) :

                # pick up the current resonance
                resonance = resonances[index]

                # set the histo style
                name = resonance.label
                linewidth = resonance.linewidth
                linestyle = resonance.linestyle
                fillstyle = resonance.fillstyle
                colour = resonance.colour

        hist.SetTitle(name)
        hist.SetName(name)
        hist.SetLineWidth(linewidth)
        hist.SetLineColor(colour)
        hist.SetLineStyle(linestyle)
        hist.SetFillColor(colour)
        hist.SetFillStyle(fillstyle)

if __name__ == "__main__" :

        logging.info("")
        logging.info("-------------------------------")
        logging.info("----------- PlotFit -----------")
        logging.info("-------------------------------")

        ###########
        ## parser
        ###########

        parser = argparse.ArgumentParser(description="Parser of the PlotFit script")

        # get the input file and tree names of the data sample
        parser.add_argument('--inFile_data_name', dest = 'inFile_data_name',
                            default = 'toy.root', help = "name of the input data sample")
        parser.add_argument('--inTree_data_name', dest = 'inTree_data_name',
                            default = 'toy', help = "tree name of the input data sample")
        parser.add_argument('--weight_data_name', dest = 'weight_data_name',
                            default = '', help = "name of the event weight of the data sample")

        # get the input file and tree names of the normalisation sample
        parser.add_argument('--inFile_norm_name', dest = 'inFile_norm_name',
                            default = 'toy.root', help = "name of the input normalisation sample")
        parser.add_argument('--inTree_norm_name', dest = 'inTree_norm_name',
                            default = 'toy_normalisation', help = "tree name of the input normalisation sample")
        parser.add_argument('--weight_norm_name', dest = 'weight_norm_name',
                            default = '', help = "name of the event weight of the normalisation sample")

        # get the input file with the fit fractions
        parser.add_argument('--inFile_fitfractions', dest = 'inFile_fitfractions',
                            default = 'FitToy_fractions.txt', help = "name of the input file with the fit fractions")

	# get the config file name where the resonances to be fitted are described
	parser.add_argument('--config_name', dest = 'config_name',
			    default = 'toy', help = "name of config file where the resonances are described")

        # set the output directory and file name
        parser.add_argument('--outFile_name', dest = 'outFile_name',
                            default = './plot_', help = "output file name")

        # in case you want to correct by an external efficiency factor,
        # additionally to what is described by the normalisation factor (like the BDT efficiency):
        # set the input file and histo names of the additional efficiency
        parser.add_argument('--inFileAdditionalEff_name', dest = 'inFileAdditionalEff_name',
                            default = "", help = "input file name of the additional efficiency factor")
        parser.add_argument('--inHistoAdditionalEff_suffix', dest = 'inHistoAdditionalEff_suffix',
                            default = "", help = "suffix of the histo name of the additional efficiency factor")

        # number of bins of the plots
        parser.add_argument('--nbins', dest = 'nbins',
                            default = 40, help = "number of bins")

        # now get the parsed options
        args = parser.parse_args()

        inFile_data_name = args.inFile_data_name
        inTree_data_name = args.inTree_data_name
        weight_data_name = args.weight_data_name

        inFile_norm_name = args.inFile_norm_name
        inTree_norm_name = args.inTree_norm_name
        weight_norm_name = args.weight_norm_name

        inFile_fitfractions = args.inFile_fitfractions

	config_name = args.config_name

        outFile_name = args.outFile_name

        inFileAdditionalEff_name = args.inFileAdditionalEff_name
        inHistoAdditionalEff_suffix = args.inHistoAdditionalEff_suffix

        nbins = args.nbins

        # print the parsed options
        logging.info("")
        logging.info("inFile_data_name = {}".format(inFile_data_name))
        logging.info("inTree_data_name = {}".format(inTree_data_name))
        logging.info("weight_data_name = {}".format(weight_data_name))
        logging.info("inFile_norm_name = {}".format(inFile_norm_name))
        logging.info("inTree_norm_name = {}".format(inTree_norm_name))
        logging.info("weight_norm_name = {}".format(weight_norm_name))
        logging.info("inFile_fitfractions = {}".format(inFile_fitfractions))
	logging.info("config_name = {}".format(config_name))
        logging.info("outFile_name = {}".format(outFile_name))
        logging.info("nbins = {}".format(nbins))
        logging.info("inFileAdditionalEff_name = {}".format(inFileAdditionalEff_name))
        logging.info("inHistoAdditionalEff_suffix = {}".format(inHistoAdditionalEff_suffix))

        ############

        nproject = 100000
        chunksize = 10000

        # let's kill the stats box
        gStyle.SetOptStat(0)

        # set and initialise the TensorFlow session
        config = tf.ConfigProto()
	config.gpu_options.allow_growth = True
        sess = tf.Session(config=config)

        # initialise the variables
        init = tf.global_variables_initializer()
	sess.run(init)

        MatrixElement.sess = sess
        PhaseSpace.sess = sess

        ########

        # import the resonances from the config file
        sys.path.append(os.path.dirname(config_name))
        resonance_module = importlib.import_module(os.path.basename(config_name)[:-3])  #-3 removes the '.py' at the end of the string

        non_res = resonance_module.non_res
        A = resonance_module.A
        B = resonance_module.B
        C = resonance_module.C

	logging.info("")
        logging.info("Imported resonances:")
        logging.info("non_res = {}".format(non_res))
	logging.info("A = {}".format([A_res.name for A_res in A]))
	logging.info("B = {}".format([B_res.name for B_res in B]))
        logging.info("C = {}\n".format([C_res.name for C_res in C]))

        resonances = A + B + C

        # let's reinitialise the variables
        init = tf.global_variables_initializer()
        sess.run(init)

        data_legend = resonance_module.data_legend

        # open the file with the additional efficiency factor... if you have to do so
        inFileAdditionalEff = None

        if inFileAdditionalEff_name != "" :
                inFileAdditionalEff = TFile.Open(inFileAdditionalEff_name)

                # check that the file exists
	        if not inFileAdditionalEff :
                        logging.error("Error: the input file of the additional efficiency does not exist.\n")
			sys.exit(1)

        #############

        # create the phase space
        phsp = PhaseSpace.ThreeBodyPhaseSpace()

        # list of switches (flags that control the components of the PDF)
        # +1 stands for the non-resonant contribution
        switches = Optimisation.Switches(len(resonances) + 1)

        def event_weight(datapoint, norm = 1.):
                return tf.transpose(datapoint)[-1] * norm # dangerous: assume that the weight is the last element of the list

        def model(datapoint, component_index = -1):
                return MatrixElement.MatrixElement(phsp, datapoint, switches,
                                                   component_index, resonances, non_res)

        # build the PDFs
        data_ph = phsp.data_placeholder
        norm_ph = phsp.norm_placeholder

        data_pdf = model(data_ph)
        norm_pdf = model(norm_ph)

        #############

        # That's how the plotting will be performed:
        # 1) use the fitted fit fractions to generate with toy simulations
        #    the contributions of the single fit components
        # 2) use the input data to show the data points in the final plots
        # 3) define the efficiency has ratio between the normalisation sample
        #    (with all the data selection applied to it) and a uniform sample generated with toy simulations
        # 3b) optional: introduce an additional efficiency factor from an external histo,
        #     for efficiency effects not described by the normalisation sample
        #     (like the BDT efficiency)
        # 4) scale the fit curves by the efficiency profiles

        #############

        # get the data, and the sweights
        unfiltered_data_sample = GetData([var[1] for var in variables] + [weight_data_name],
                                         inFile_data_name, inTree_data_name)

        # filter the data, checking for the allowed phase space
        data_sample = sess.run(phsp.Filter(unfiltered_data_sample))

        # get the normalisation sample, and the weights
        unfiltered_norm_sample = GetData([var[1] for var in variables] + [weight_norm_name],
                                         inFile_norm_name, inTree_norm_name)

        # filter the normalisation sample, checking for the allowed phase space
        norm_sample = sess.run(phsp.Filter(unfiltered_norm_sample))

        # get the fit fractions
        fit_fractions = Optimisation.ReadFitFractions(inFile_fitfractions,
                                                      [res.name for res in resonances] +  ["non-resonant"])

        # print the B_LS couplings of the resonances, for debug
        for res in resonances:
                MatrixElement.print_BSL(res.BSL_M, res.name + "__M")
	        MatrixElement.print_BSL(res.BSL_res, res.name + "__res")

        ########
        # start with the fun: plot the fit projections
        ########

        majorant = ToyMC.EstimateMaximum(sess, data_pdf, data_ph, norm_sample) * 1.1

        # generate a fit sample, i.e. a toy MC with the input fit fractions
        fit_sample = ToyMC.RunToyMC(sess, data_pdf, data_ph, phsp,
                                    nproject, majorant, chunk = chunksize,
                                    seed = 1000)

        # components of the fit model
        component_samples = []

        # loop over the fit components to generate
        for i in range(len(fit_fractions)):

                name = getname(i, resonances)

                # only generate the components with non null fit fractions
                if int(len(fit_sample)*fit_fractions[i]) > 0:

                        logging.info("Generating the {} component".format(name))

                        # build the PDF
                        component_pdf = model(data_ph, i)

                        majorant = ToyMC.EstimateMaximum(sess, component_pdf, data_ph, norm_sample) * 1.1

                        # generate the component
                        component_samples += [ToyMC.RunToyMC(sess, component_pdf, data_ph, phsp,
                                                             int(len(fit_sample) * fit_fractions[i]),
                                                             majorant, chunk = chunksize,
                                                             seed = 1000)]
                else:
                        logging.info("Won't generate toys for {} since the fit fraction is too small = {}".format(name, str(fit_fractions[i])))

                        component_samples += [[[]]]

        # let's add the other variables that you want to plot
        variables += other_variables

        # components to be plotted (only the ones with non null fit fraction)
        components_tobe_plotted = []

        ###########
        # build the data sample (data points) and the fit sample (fit curves)
        ###########

        # define a weighted sample that will be filled with the input data
        # the weight is the one in the input dataset (aka the sweight, for the fitted data)
        sample_weighted = AddVariables_ToSample(data_ph, phsp, variables, weighted = True)

        # fill the data sample: it will represent the data points in the final plots
        data_sample = sess.run(sample_weighted, {data_ph: data_sample})


        # define a not-weighted sample (i.e., with a weight == 1.)
        sample_notweighted = AddVariables_ToSample(data_ph, phsp, variables, weighted = False)

        # fill the fit sample: it will represent the fit curve in the final plots
        fit_sample = sess.run(sample_notweighted, {data_ph: fit_sample})

        ###########

        # fill the single components
        for i in range(len(component_samples)):
                name = getname(i, resonances)

                if len(component_samples[i]) > 1:

                        # add to the list of components to be plotted
                        if i < len(resonances) :
                                components_tobe_plotted += [resonances[i].label]
                        else :
                                components_tobe_plotted += ["non-resonant"]

                        component_samples[i] = sess.run(sample_notweighted, {data_ph: component_samples[i]})
                else:
                        logging.info("Skipping {}".format(name))

        # generate a uniform sample
        unif_sample = sess.run(phsp.UniformSample(nproject))

        norm_sample = sess.run(sample_weighted, {data_ph: norm_sample})
        unif_sample = sess.run(sample_notweighted, {data_ph: unif_sample})

        # loop over the variables to plot
        for i in range(len(variables)):

                var = variables[i]

                # define the canvas
                can = TCanvas(var[1] + "can", "", 800, 600)
                can.Draw()
                can.cd()

                # define the main pad
                mainpad = TPad(can.GetName() + "main", "", 0.0, 0.2, 1.0, 1.)

                mainpad.SetMargin(0.11, 0.05, 0.03, 0.05)
                mainpad.Draw()
                mainpad.cd()
                StyleCanvas(can)

                # histo with the data points
                data_hist = TH1D(var[1] + "data", "", nbins, var[2], var[3])

                # histo with the fit curves
                fit_hist = TH1D(var[1] + "model", "", nbins, var[2], var[3])

                # efficiency histo
                eff_hist = TH1D(var[1] + "norm", "", nbins, var[2], var[3])

                # whatever histo
                unif_hist = TH1D(var[1] + "unif", "", nbins, var[2], var[3])

                # array of histos for the single fit components
                # add a separate histo for each of the component
                comp_hists = []

                for j in range(len(fit_fractions)):
                        name = getname(j, resonances)

                        # define a new histo
                        comp_hists += [TH1D(var[1] + name, "", nbins, var[2], var[3])]

                # fill all the histos in two shots
                for hist, sample in [[data_hist, data_sample],
                                     [fit_hist, fit_sample],
                                     [eff_hist, norm_sample],
                                     [unif_hist, unif_sample]]:
                        for dp in sample:
                                hist.Fill(dp[i], dp[-1])

                for j in range(len(component_samples)):
                        if len(component_samples[j]) > 1:
                                for dp in component_samples[j][:,i]:
                                        comp_hists[j].Fill(dp)

                                StyleComponentHist(comp_hists[j], j, resonances)

                # calculate the efficiency
                # eff = normalisation sample / uniform sample
                # the normalisation and uniform histos must be normalised before
                eff_hist.Scale(1./eff_hist.Integral())
                unif_hist.Scale(1./unif_hist.Integral())
                eff_hist.Divide(unif_hist)

                # multiply the efficiency by an external efficiency factor,
                # to take into account efficiency effects not described by the normalisation sample
                # (like the BDT efficiency)
                if inFileAdditionalEff != None :
                        inHistoEff_name = var[1] + inHistoAdditionalEff_suffix

                        # open the efficiency histo
                        AdditionalEffHisto = inFileAdditionalEff.Get(inHistoEff_name)

                        # check that the histo exists
                        if not AdditionalEffHisto :
                                logging.error("Error: the efficiency histo {} does not exist.\n".format(inHistoEff_name))
                                sys.exit(1)

                        # multiply the efficiency from the normalisation sample
                        # with the additional efficiency factor
                        eff_hist.Multiply(AdditionalEffHisto)

                # normalisation factor to overimpose the data point and the fit curves
                scale = data_hist.Integral()/fit_hist.Integral()

                # some style
                fit_hist.SetLineColor(2)
                fit_hist.SetLineWidth(2)

                StyleHist(data_hist, var[4], var[5])
                data_hist.SetLineColor(1)
                data_hist.SetMarkerStyle(20)

                data_hist.GetXaxis().SetLabelSize(0)

                data_hist.GetYaxis().SetTitleSize(0.055)
                data_hist.GetYaxis().SetTitleOffset(0.9)
                data_hist.GetYaxis().SetLabelSize(0.05)

                data_hist.Draw("E1")
                data_hist.SetMinimum(0)

                #define the legend
                legend = TLegend(0.72, 0.2, 0.9, 0.9, "LHCb internal", "brNDC")

                legend.AddEntry(data_hist, data_legend, "elp")

                # loop over the histos of the single fit contributions + histo with the total fit
                for hist in [fit_hist]+comp_hists:

                        # to select the components which should be plotted
			to_be_plotted = False

			if "model" in hist.GetName() :
                                legend.AddEntry(hist, "model", "l")
                                to_be_plotted = True
			else :
                                for comp in components_tobe_plotted :
                                        if comp in hist.GetName() :
                                                legend.AddEntry(hist, hist.GetName(), "f")
		                                to_be_plotted = True

			if to_be_plotted :
                                #print "Plotting hist =", hist.GetName()

                                # multiply the fit model and contributions by the efficiency
                                hist.Multiply(eff_hist)
		                hist.Scale(scale)
				hist.Draw("h same")

                legend.SetBorderSize(0);
                legend.SetFillColor(kWhite);
                legend.SetTextAlign(12);
                legend.SetFillStyle(0);

                legend.Draw()

                can.cd()

                # plot the pull distribution
                pullpad = TPad(can.GetName() + "pull", "", 0.0, 0., 1.0, 0.21)
                pullpad.SetMargin(0.11, 0.05, 0.4, 0.05)
                pullpad.Draw()
                pullpad.cd()

                # compute the pulls
                pullplot = PullPlot(fit_hist, data_hist)

                StyleHist(pullplot, var[4], var[5])

                pullplot.GetYaxis().SetTitle("Pull")

                pullplot.GetXaxis().SetTitleSize(0.2)
                pullplot.GetXaxis().SetTitleOffset(0.8)

                pullplot.GetXaxis().SetLabelSize(0.18)

                pullplot.GetYaxis().SetTitleSize(0.2)
                pullplot.GetYaxis().SetTitleOffset(0.25)
                pullplot.GetYaxis().SetLabelSize(0.18)

                pullplot.Draw("BX")

                #draw a horizontal line, corresponding to +3 sigma
                plus_three_sigma = TLine(pullplot.GetXaxis().GetXmin(), 3.,   #xmin, ymin
                                         pullplot.GetXaxis().GetXmax(), 3.)   #xmax, ymax

                #red, dashed line
                plus_three_sigma.SetLineStyle(7)
                plus_three_sigma.SetLineColor(kBlack)
                plus_three_sigma.Draw("same")

                #draw a horizontal line, corresponding to -3 sigma
                minus_three_sigma = TLine(pullplot.GetXaxis().GetXmin(), -3.,   #xmin, ymin
                                          pullplot.GetXaxis().GetXmax(), -3.)   #xmax, ymax

                #red, dashed line
                minus_three_sigma.SetLineStyle(7)
                minus_three_sigma.SetLineColor(kBlack)
                minus_three_sigma.Draw("same")

                #########

                data_hist.SetMaximum(max(data_hist.GetMaximum(), fit_hist.GetMaximum())*1.1)

                # replot the main pad, to draw correctly the axis
                can.cd()
                mainpad.Draw()

                # save the plot to an external file
                out_path = outFile_name + "_" + var[1] + ".pdf"

                logging.info("Plotting {}".format(out_path))

                can.SaveAs(out_path)

                ##########

                # draw and save the efficiency plot as well
                can.Clear()

                StyleHist(eff_hist, var[4], var[5])
                eff_hist.SetLineColor(1)
                eff_hist.SetMarkerStyle(20)

                eff_hist.GetXaxis().SetLabelSize(0)

                eff_hist.GetYaxis().SetTitle("efficiency")
                eff_hist.GetYaxis().SetTitleSize(0.055)
                eff_hist.GetYaxis().SetTitleOffset(0.9)
                eff_hist.GetYaxis().SetLabelSize(0.05)

                eff_hist.SetMinimum(0.5)
                eff_hist.SetMaximum(1.5)

                can.cd()
                eff_hist.Draw()

                out_path = outFile_name + "_" + var[1] + "_efficiency.pdf"

                can.SaveAs(out_path)

        # finally close the session
        sess.close()


import sys

#import the local libraries
from Constants import *
import MatrixElement
import Optimisation
from Interface import Const
from Optimisation import FitParameter

# label of the data legend
data_legend = "Run I + Run II data"

# non-resonant component
non_res = MatrixElement.NonResonant(nonres_size = FitParameter("nonres_size", 1., 0., 10., 0.01),
                                    colour = kPink, linestyle = 7, fillstyle = 3954)

# list of Lc D0 resonances
A = [
]

# list of D0 K resonances
B = [
    # Lb --> Lc Ds0*(2317)  --> removed because it's a scalar (J = 0)
    #MatrixElement.BResonanceBreitWigner(name = "Ds0*_2317", label = "D_{s0}^{*}(2317)", mass = Const(2.318), width = Const(0.0038),
    #                                    spin = 0, parity = +1, aboveLmin = 1,
    #                                    use_BLS_RealAndImag = True,
    #                                    resdecay_parity_conservation = True,
    #                                    colour = kBrown, linestyle = 7, fillstyle = 3954),

    # Lb --> Lc Ds1(2460)  --> removed because the parity conservation does not allow any B_LS coupling of the resonance decay
    #MatrixElement.BResonanceBreitWigner(name = "Ds1_2460", label = "D_{s1}(2460)", mass = Const(2.460), width = Const(0.0035),
    #                                    spin = 2, parity = +1, aboveLmin = 1,
    #                                    use_BLS_RealAndImag = True,
    #                                    resdecay_parity_conservation = True,
    #                                    colour = kBrown, linestyle = 7, fillstyle = 3954),

    # Lb --> Lc Ds1(2536)  --> removed because the parity conservation does not allow any B_LS coupling of the resonance decay
    #MatrixElement.BResonanceBreitWigner(name = "Ds1_2536", label = "D_{s1}(2536)", mass = Const(2.535), width = Const(0.001),
    #                                    spin = 2, parity = +1, aboveLmin = 1,
    #                                    use_BLS_RealAndImag = True,
    #                                    resdecay_parity_conservation = True,
    #                                    colour = kBrown, linestyle = 7, fillstyle = 3954),
    
    # Lb --> Lc Ds2*(2573)
    #MatrixElement.BResonanceBreitWigner(name = Ds2_2573.name, label = Ds2_2573.label,
    #                                    mass = Ds2_2573.mass, width = Ds2_2573.width,
    #                                    spin = Ds2_2573.spin, parity = Ds2_2573.parity, aboveLmin = 0,
    #                                    use_BLS_RealAndImag = True,
    #                                    BSLh_Lb = {(Ds2_2573.name + "__M_Bs1l0hall_Real") : FitParameter((Ds2_2573.name + "__M_Bs1l0hall_Real"), 0.5, 0., 1., 0.01),
    #                                               (Ds2_2573.name + "__M_Bs1l0hall_Imag") : FitParameter((Ds2_2573.name + "__M_Bs1l0hall_Imag"), 0.5, 0., 1., 0.01)},
    #                                    resdecay_parity_conservation = True,
    #                                    colour = kAzure, linestyle = 7, fillstyle = 3954),

    # Lb --> Lc Ds1*(2700)
    MatrixElement.BResonanceBreitWigner(name = Ds1_2700.name, label = Ds1_2700.label,
                                        mass = Ds1_2700.mass, width = Ds1_2700.width,
                                        spin = Ds1_2700.spin, parity = Ds1_2700.parity, aboveLmin = 0,
                                        use_BLS_RealAndImag = True,
                                        couplings_dict_M = {
                                            "Ds1_2700__M_Bs1l0hall_Real"  : Const(1.), #FitParameter("Ds1_2700__M_Bs1l0hall_Real", 0.5, -10., 10., 0.01),
                                            "Ds1_2700__M_Bs1l0hall_Imag"  : Const(0.), #FitParameter("Ds1_2700__M_Bs1l0hall_Imag", 0.5, -10., 10., 0.01),
                                            #"Ds1_2700__M_Bs1l2hall_Real"  : FitParameter("Ds1_2700__M_Bs1l2hall_Real", 0.5, -10., 10., 0.01), #aboveLmin = 2
                                            #"Ds1_2700__M_Bs1l2hall_Imag"  : FitParameter("Ds1_2700__M_Bs1l2hall_Imag", 0.5, -10., 10., 0.01), #aboveLmin = 2
                                            #"Ds1_2700__M_Bs3l2hall_Real"  : FitParameter("Ds1_2700__M_Bs3l2hall_Real", 0.5, -10., 10., 0.01), #aboveLmin = 2
                                            #"Ds1_2700__M_Bs3l2hall_Imag"  : FitParameter("Ds1_2700__M_Bs3l2hall_Imag", 0.5, -10., 10., 0.01), #aboveLmin = 2
                                        },                                        
                                        couplings_dict_res = {
                                            #"Ds1_2700__res_Bs0l2h-2_Real" : Const(1.0),
                                            #"Ds1_2700__res_Bs0l2h-2_Imag" : Const(0.),
                                            "Ds1_2700__res_Bs0l2h-2_Real" : FitParameter("Ds1_2700__res_Bs0l2h-2_Real", 0.5, -10., 10., 0.01),
                                            "Ds1_2700__res_Bs0l2h-2_Imag" : FitParameter("Ds1_2700__res_Bs0l2h-2_Imag", 0.5, -10., 10., 0.01),
                                            "Ds1_2700__res_Bs0l2h0_Real"  : FitParameter("Ds1_2700__res_Bs0l2h0_Real", 0.5, -10., 10., 0.01),
                                            "Ds1_2700__res_Bs0l2h0_Imag"  : FitParameter("Ds1_2700__res_Bs0l2h0_Imag", 0.5, -10., 10., 0.01),
                                            "Ds1_2700__res_Bs0l2h2_Real"  : FitParameter("Ds1_2700__res_Bs0l2h2_Real", 0.5, -10., 10., 0.01),
                                            "Ds1_2700__res_Bs0l2h2_Imag"  : FitParameter("Ds1_2700__res_Bs0l2h2_Imag", 0.5, -10., 10., 0.01)},
                                        resdecay_parity_conservation = True, 
                                        colour = kBrown, linestyle = 7, fillstyle = 3954),

    # Lb --> Lc Ds1*(2860)
    #MatrixElement.BResonanceBreitWigner(name = Ds1_2860.name, label = Ds1_2860.label,
    #                                    mass = Ds1_2860.mass, width = Ds1_2860.width,
    #                                    spin = Ds1_2860.spin, parity = Ds1_2860.parity, aboveLmin = 0,
    #                                    use_BLS_RealAndImag = True,
    #                                    BSLh_M = {(Ds1_2860.name + "__M_Bs1l0hall_Real")  : FitParameter((Ds1_2860.name + "__M_Bs1l0hall_Real"), 0.5, -10., 10., 0.01),
    #                                              (Ds1_2860.name + "__M_Bs1l0hall_Imag")  : FitParameter((Ds1_2860.name + "__M_Bs1l0hall_Imag"), 0.5, -10., 10., 0.01)},
    #                                    BSLh_res = {(Ds1_2860.name + "__res_Bs0l2h-2_Real") : Const(1.0),
    #                                                (Ds1_2860.name + "__res_Bs0l2h-2_Imag") : Const(0.),
    #                                                (Ds1_2860.name + "__res_Bs0l2h0_Real")  : FitParameter((Ds1_2860.name + "__res_Bs0l2h0_Real"), 0.5, -10., 10., 0.01),
    #                                                (Ds1_2860.name + "__res_Bs0l2h0_Imag")  : FitParameter((Ds1_2860.name + "__res_Bs0l2h0_Imag"), 0.5, -10., 10., 0.01),
    #                                                (Ds1_2860.name + "__res_Bs0l2h2_Real")  : FitParameter((Ds1_2860.name + "__res_Bs0l2h2_Real"), 0.5, -10., 10., 0.01),
    #                                                (Ds1_2860.name + "__res_Bs0l2h2_Imag")  : FitParameter((Ds1_2860.name + "__res_Bs0l2h2_Imag"), 0.5, -10., 10., 0.01)},
    #                                    resdecay_parity_conservation = True,
    #                                    colour = kGreen, linestyle = 7, fillstyle = 3954),

    # Lb --> Lc Ds3*(2860)
    #MatrixElement.BResonanceBreitWigner(name = "Ds3*_2860", label = "D_{s3}^{*}(2860)", mass = Const(2.860), width = Const(0.053),
    #                                    spin = 6, parity = -1, aboveLmin = 1,
    #                                    use_BLS_RealAndImag = True,
    #                                    fitparams_BLS_Lb_real = [0.1, -2., 2.], fitparams_BLS_Lb_imag = [0., -2., 2.],
    #                                    fixBLS_res_toone = True,
    #                                    resdecay_parity_conservation = True,
    #                                    colour = kCyan, linestyle = 7, fillstyle = 3954),

    # Lb --> Lc DsJ(3040)
    MatrixElement.BResonanceBreitWigner(name = Dsj_3040.name, label = Dsj_3040.label,
                                        mass = Dsj_3040.mass, width = Dsj_3040.width,
                                        spin = Dsj_3040.spin, parity = Dsj_3040.parity, aboveLmin = 0,
                                        use_BLS_RealAndImag = True,
                                        couplings_dict_M = {
                                            "DsJ_3040__M_Bs1l0hall_Real" : Const(1.), #FitParameter("DsJ_3040__M_Bs1l0hall_Real", 0.5, -10., 10., 0.01),
                                            "DsJ_3040__M_Bs1l0hall_Imag" : Const(0.), #FitParameter("DsJ_3040__M_Bs1l0hall_Imag", 0.5, -10., 10., 0.01),
                                        },
                                        couplings_dict_res = {
                                            #"DsJ_3040__res_Bs0l2h-2_Real" : Const(1.),
                                            #"DsJ_3040__res_Bs0l2h-2_Imag" : Const(0.),
                                            "DsJ_3040__res_Bs0l2h-2_Real" : FitParameter("DsJ_3040__res_Bs0l2h-2_Real", 0.5, -10., 10., 0.01),
                                            "DsJ_3040__res_Bs0l2h-2_Imag" : FitParameter("DsJ_3040__res_Bs0l2h-2_Imag", 0.5, -10., 10., 0.01),
                                            "DsJ_3040__res_Bs0l2h0_Real"  : FitParameter("DsJ_3040__res_Bs0l2h0_Real", 0.5, -10., 10., 0.01),
                                            "DsJ_3040__res_Bs0l2h0_Imag"  : FitParameter("DsJ_3040__res_Bs0l2h0_Imag", 0.5, -10., 10., 0.01),
                                            "DsJ_3040__res_Bs0l2h2_Real"  : FitParameter("DsJ_3040__res_Bs0l2h2_Real", 0.5, -10., 10., 0.01),
                                            "DsJ_3040__res_Bs0l2h2_Imag"  : FitParameter("DsJ_3040__res_Bs0l2h2_Imag", 0.5, -10., 10., 0.01)
                                        },
                                        resdecay_parity_conservation = True,
                                        colour = kBlue, linestyle = 7, fillstyle = 3954),
]

# list of Lc K resonances
C = [    
    # Lb --> Xic(2790) D0
    #MatrixElement.CResonanceBreitWigner(name = Xic_2790.name, label = Xic_2790.label,
    #                                    mass = Xic_2790.mass, width = Const(0.015),
    #                                    spin = Xic_2790.spin, parity = Xic_2790.parity, aboveLmin = 0,
    #                                    use_BLS_RealAndImag = True,
    #                                    couplings_dict_M = {
    #                                        "Xic_2790__M_Bs1l0hall_Real"  : Const(1.), #FitParameter("Xic_2790__M_Bs1l0hall_Real", 0.5, -10., 10., 0.01),
    #                                        "Xic_2790__M_Bs1l0hall_Imag"  : Const(0.), #FitParameter("Xic_2790__M_Bs1l0hall_Imag", 0.5, -10., 10., 0.01)
    #                                    },
    #                                    couplings_dict_res = {
    #                                        "Xic_2790__res_Bs1l0h-1_Real" : FitParameter("Xic_2790__res_Bs1l0h-1_Real", 0.5, -10., 10., 0.01),
    #                                        "Xic_2790__res_Bs1l0h-1_Imag" : FitParameter("Xic_2790__res_Bs1l0h-1_Imag", 0.5, -10., 10., 0.01),
    #                                        #"Xic_2790__res_Bs1l0h1_Real"  : "Xic_2790__res_Bs1l0h-1_Real",
    #                                        #"Xic_2790__res_Bs1l0h1_Imag"  : "Xic_2790__res_Bs1l0h-1_Imag",
    #                                        "Xic_2790__res_Bs1l0h1_Real"  : FitParameter("Xic_2790__res_Bs1l0h1_Real", 0.5, -10., 10., 0.01),
    #                                        "Xic_2790__res_Bs1l0h1_Imag"  : FitParameter("Xic_2790__res_Bs1l0h1_Imag", 0.5, -10., 10., 0.01)
    #                                    },
    #                                    resdecay_parity_conservation = True,
    #                                    colour = kCyan, linestyle = 7, fillstyle = 3954),

    # Lb --> Xic(2815) D0
    #MatrixElement.CResonanceBreitWigner(name = Xic_2815.name, label = Xic_2815.label,
    #                                    mass = Xic_2815.mass, width = Const(0.015),
    #                                    spin = Xic_2815.spin, parity = Xic_2815.parity, aboveLmin = 0,
    #                                    use_BLS_RealAndImag = True,
    #                                    couplings_dict_M = {
    #                                        "Xic_2815__M_Bs3l2hall_Real"  : Const(1.), #FitParameter("Xic_2815__M_Bs3l2hall_Real", 0.5, -10., 10., 0.01),
    #                                        "Xic_2815__M_Bs3l2hall_Imag"  : Const(0.), #FitParameter("Xic_2815__M_Bs3l2hall_Imag", 0.5, -10., 10., 0.01)
    #                                    },
    #                                    couplings_dict_res = {
    #                                        "Xic_2815__res_Bs1l4h-1_Real" : FitParameter("Xic_2815__res_Bs1l4h-1_Real", 0.5, -10., 10., 0.01),
    #                                        "Xic_2815__res_Bs1l4h-1_Imag" : FitParameter("Xic_2815__res_Bs1l4h-1_Imag", 0.5, -10., 10., 0.01),
    #                                        #"Xic_2815__res_Bs1l4h1_Real"  : "Xic_2815__res_Bs1l4h-1_Real",
    #                                        #"Xic_2815__res_Bs1l4h1_Imag"  : "Xic_2815__res_Bs1l4h-1_Imag",
    #                                        "Xic_2815__res_Bs1l4h1_Real"  : FitParameter("Xic_2815__res_Bs1l4h1_Real", 0.5, -10., 10., 0.01),
    #                                        "Xic_2815__res_Bs1l4h1_Imag"  : FitParameter("Xic_2815__res_Bs1l4h1_Imag", 0.5, -10., 10., 0.01)
    #                                    },
    #                                    resdecay_parity_conservation = True,
    #                                    colour = kMagenta, linestyle = 7, fillstyle = 3954),
]

#############

# additional C resonances which are enabled/disabled or modified
# by the calling rules
# the behaviour is driven by those variables
Xic_2930_enabled = False #XIC_2930_ENABLED
Xic_2970_enabled = False #XIC_2970_ENABLED
Xic_3055_enabled = False
Xic_3080_enabled = False #XIC_3080_ENABLED

if Xic_2930_enabled :
    C += [
        # Lb --> Xic(2930) D0   # unknown J^p
        # from https://arxiv.org/pdf/1712.03612.pdf
        MatrixElement.CResonanceBreitWigner(name = Xic_2930.name, label = Xic_2930.label,
                                            mass = Xic_2930.mass, width = Xic_2930.width,
                                            spin = XIC_2930_SPIN, parity = XIC_2930_PARITY, aboveLmin = 0,
                                            resdecay_parity_conservation = True,
                                            colour = kPink, linestyle = 7, fillstyle = 3954)
    ]
    
if Xic_2970_enabled :
    C += [
        # Lb --> Xic(2970) D0   # unknown J^p
        MatrixElement.CResonanceBreitWigner(name = Xic_2970.name, label = Xic_2970.label,
                                            mass = Xic_2970.mass, width = Xic_2970.width,
                                            spin = XIC_2970_SPIN, parity = XIC_2970_PARITY, aboveLmin = 0,
                                            resdecay_parity_conservation = True,
                                            colour = kPink, linestyle = 7, fillstyle = 3954)
    ]

#if Xic_3055_enabled :
#    C += [
#        # Lb --> Xic(3055) D0   # unknown J^p # also, there is no neutral-charged version
#        MatrixElement.CResonanceBreitWigner(name = "Xic_3055", label = "#Xi_{c}(3055)", mass = Const(3.056), width = Const(0.008),
#                                            spin = XIC_3055_SPIN, parity = XIC_3055_PARITY, aboveLmin = 0,
#                                            resdecay_parity_conservation = True,
#                                            colour = kPink, linestyle = 7, fillstyle = 3954)
#    ]

if Xic_3080_enabled :
    C += [
        # Lb --> Xic(3080) D0   # unknown J^p
        MatrixElement.CResonanceBreitWigner(name = Xic_3080.name, label = Xic_3080.label,
                                            mass = Xic_3080.mass, width = Xic_3080.width,
                                            spin = XIC_3080_SPIN, parity = XIC_3080_PARITY, aboveLmin = 0,
                                            resdecay_parity_conservation = True,
                                            colour = kViolet, linestyle = 7, fillstyle = 3954),
    ]

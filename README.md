# TensorFlow Amplitude Models

This package provides basics scripts to run toy simulations using tensor flow analysis.
Make a local clone of this repository to use them. Make sure to also pull the submodules
```
git clone --recursive ssh://git@gitlab.cern.ch:7999/sneubert/tfamplmodels.git
```
If you cloned without the recursive flag you can pull the submodules by doing
```
git submodule update --init --recursive
```

This repo is designed to be used with the TensorFlow-docker image to provide all dependencies.
To pull this docker image, log into the CERN registry and pull
```bash
docker login gitlab-registry.cern.ch
docker pull gitlab-registry.cern.ch/sneubert/tensorflow-docker
```

# Generate toys

The scripts will be mounted into the docker container and then the code can be executed inside.
To do this start the container from the toplevel directory of this repository like so:

```bash
docker run -v $PWD:/code --rm -it gitlab-registry.cern.ch/sneubert/tensorflow-docker bash
```

This should drop you inside the container into the `/tmp` directory. To run the simulation do
```bash
export PYTHONPATH=$PYTHONPATH:/code/externals/TensorFlowAnalysis/TensorFlowAnalysis/:/code/config/:/code/config/amplitude_fits/:/code/include/:/code/config/amplitude_fits/
cd /code/amplitude_scripts
python2.7 GenerateToys.py --n_events 5000 --n_events_norm 20000
```

The model is specified in `config/resonances_genToyMC.py`

Resonances that can be used are specified in `include/Constants.py`


# Run the amplitude_fits
Start the docker container as shown in the previous section, setup the environment and go into the `amplitude_scripts` directory.

To start the fit do:
```bash
python2.7 Fit.py --inFile_data_name toy.root --inTree_data_name toy --weight_data_name NLb_sw --inFile_norm_name toy.root --inTree_norm_name toy_normalisation --weight_norm_name weight --config_name ../config/resonances_fitpKpi.py --outFile fitResult
```

This can take a while.

After the fit has finished, transfer the results into a new config file using the `makePlotConfig.py` tool. If you want to change file names
check inside that short script. Also note that the template file `config/resonances_plotLc2ppiK`.

When the fit has ended, you can visualize the results like so:
```bash
python2.7 PlotFit.py --inFile_data_name toy.root --inTree_data_name toy --weight_data_name NLb_sw --inFile_norm_name toy.root --inTree_norm_name toy_normalisation --weight_norm_name weight --config_name ../config/resonances_plotpKpi_TEMP.py --inFile_fitfractions fitResult_fractions.info
```


Contact Person : Sebastian Neubert   
Contact Email  : sneubert@cern.ch  

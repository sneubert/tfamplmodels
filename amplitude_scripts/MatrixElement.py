
import tensorflow as tf
import pypdt
import ROOT
import sys
import math

#import the local libraries
import Kinematics
import Dynamics

from Optimisation import FitParameter, PrintParameters
import Interface
from Interface import Const, Complex, CastComplex, Acos, Clebsch, Ones

from Constants import *

# import the Python settings
from python_settings import *

########
# DISCLAIMER:
# The current implementation of the matrix elements
# rely on the fact that you have a (spin 1/2) --> (spin 1/2) scalar scalar decay,
# with d1 being the daughter particle with non null spin
# If you try to use this exact implementation for a decay with different spin structures
# do that at your own risk!
########

######

# some global variables

# step size of the fit parameters
step_size = 0.01

sess = None

fitted_values = None

# helicities that can be taken by M and d1:
# essentially, just -1 and +1
M_helicities =	range(-M.spin, M.spin + 1, 2)
d1_helicities = range(-d1.spin, d1.spin + 1, 2)

######

def GetL(spin, parity) :
	l1 = (spin-1)/2     # Lowest possible momentum
	p1 = 2*(l1 % 2)-1   # P=(-1)^(L1+1), e.g. P=-1 if L=0

	if p1 == parity : return l1

        return l1+1

def GetSvalues(JB, JC):
	"""
	   Return a set of discrete spin values in |JB - JC| <= S <= JB + JC
	   This assumes you're working with doubled angular momentum numbers
	"""

        return range(abs(JB - JC), JB + JC + 1, 2)

def SanN(n):
	"""
           Turn a signed integer into an alphanumeric string +1 -> "p1", -3 -> "m3"
	"""

        if n < 0:
		return "m" + str(-n)
	elif n > 0:
		return "p" + str(n)
	else:
		return "0"

# get the helicity amplitude
def GetHelAmp(J_0, hel_0,
              J_1, hel_1,
              J_2, hel_2,
              B_SL):

        HelAmp = CastComplex(Const(0.))

        # loop over the S, L and resonance helicity values
        for S in B_SL.keys():
                for L in B_SL[S].keys():

                        logging.debug("S = {}, L = {}, hel = {}".format(S, L, hel_0))

                        HelAmp = (B_SL[S][L][hel_0] * math.sqrt((L + 1.)/(J_0 + 1.)) *
                                  complex(Clebsch(J_1, hel_1,
                                                  J_2, -hel_2,
                                                  S, hel_1 - hel_2),
                                          0) *
                                  complex(Clebsch(L, 0,
                                                  S, hel_1 - hel_2,
                                                  J_0, hel_1 - hel_2),
                                          0)
                        )

        return HelAmp

# get all the possible B_LS couplings for the A --> B C decay
# you have to reduce the number of needed couplings later!
def get_all_possible_BSL(J_A, J_B, J_C, helicities = None,
                         parity_conserved = False,
                         P_A = +1, P_B = +1, P_C = +1):

        L_values = []

        # get the S values of the daughters combination
        S_values = GetSvalues(J_B, J_C)

        B_SL = {}

        # loop over the allowed S values of the daughters combination
        # I first loop over S, because I need it to compute the L values
        for S in S_values:

                B_SL[S] = {}

                # loop over the allowed L = J_A + S of the decay
                for L in range(abs(J_A - S), J_A + S + 1, 2):

                        # if the parity is conserved in the decay,
                        # check that the current L is allowed
                        if parity_conserved :
                                if (P_B * P_C * (-1)**(L/2)) != P_A:
                                        continue

                        L_values += [L]

                        B_SL[S][L] = {}

                        # check if I have to add an helicity dependence to the B_LS couplings
                        if helicities is None :
                                # no dependence on the helicity: let's flag it as "all"
                                B_SL[S][L]["all"] = CastComplex(Const(1.))
                        else :
                                # dependence on the helicities!

                                # loop over the helicities
                                for hel in helicities:
                                        B_SL[S][L][hel] = CastComplex(Const(1.))

        return S_values, L_values, B_SL

# get the L orbital angular momenta between the resonance
# and the spectator particle
def GetLvalues_res_spectator(J_res, J_spectator):

        L_values = []

        # get the S values of the resonance + spectator
        S_values = GetSvalues(J_res, J_spectator)

        # loop over the allowed S values of the resonance + spectator combination
        for S in S_values:

                # loop over the allowed L = J_M + S of the M decay
                for L in range(abs(M.spin - S), M.spin + S + 1, 2):

                        # if the parity is conserved in the decay,
                        # check that the current L is allowed
                        # but the M decays weakly, so I'm sorry but no P conservation
                        #if parity_conserved :
                        #        if (P_B * P_C * (-1)**(L/2)) != P_A:
                        #                continue

                        L_values += [L]

        return L_values

##############################

# Stuff for the B_LS couplings

# print the B_LS couplings, for debug purposes
def print_BSL(B_SL, coupling_name) :

        # loop over the L and S values
        for S in B_SL.keys():
                for L in B_SL[S].keys():
                        for hel in B_SL[S][L].keys():

                                # build the coupling name
                                name_parameter = coupling_name + "_Bs" + str(S) + "l" + str(L) + "h" + str(hel)

                                # print the values
                                logging.info("print_BSL: {} = {}".format(name_parameter, sess.run(B_SL[S][L][hel])))

        return

# filter the allowed couplings, and returns their number
def filter_allowed_couplings(B_SL, allowedL):

        num_couplings = 0

        # loop over the S, L and helicity values
        for S in B_SL.keys():
                for L in B_SL[S].keys():

                        # only accept the B_SL couplings with the allowed L values
                        # delete the the ones not allowed
                        if not L in allowedL:
                                logging.debug("filter_allowed_couplings: Deleting B_SL[L = {}, S = {}]".format(S, L))

                                del B_SL[S][L]

                                continue

                        num_couplings += 1

        return num_couplings

# save all the BLS couplings to an output file,
# in cartesian coordinates
def SaveBSL_cartesian(resonance, outFile) :

        # print the B_LS couplings of the M decay
        for S in resonance.BSL_M.keys():
                for L in resonance.BSL_M[S].keys():
                        for hel in resonance.BSL_M[S][L].keys():

                                name = resonance.name + "__M_Bs" + str(S) + "l" + str(L) + "h" + str(hel)

                                # write the magnitude
                                outFile.write(name + "_Real\t" + str(sess.run(Interface.Real(resonance.BSL_M[S][L][hel]))) + "\n")

                                # write the phase
                                outFile.write(name + "_Imag\t" + str(sess.run(Interface.Imaginary(resonance.BSL_M[S][L][hel]))) + "\n")

        # print the B_LS couplings of the resonance decay
        for S in resonance.BSL_res.keys():
                for L in resonance.BSL_res[S].keys():
                        for hel in resonance.BSL_res[S][L].keys():

                                name = resonance.name + "__res_Bs" + str(S) + "l" + str(L) + "h" + str(hel)

                                # write the magnitude
                                outFile.write(name + "_Real\t" + str(sess.run(Interface.Real(resonance.BSL_res[S][L][hel]))) + "\n")

                                # write the phase
                                outFile.write(name + "_Imag\t" + str(sess.run(Interface.Imaginary(resonance.BSL_res[S][L][hel]))) + "\n")

        return

# save all the BLS couplings to an output file,
# in polar coordinates
def SaveBSL_polar(resonance, outFile) :

        # print the B_LS couplings of the M decay
        for S in resonance.BSL_M.keys():
                for L in resonance.BSL_M[S].keys():
                        for hel in resonance.BSL_M[S][L].keys():

                                name = resonance.name + "__M_Bs" + str(S) + "l" + str(L) + "h" + str(hel)

                                # write the magnitude
                                outFile.write(name + "_Mag\t" + str(sess.run(Interface.Mag(resonance.BSL_M[S][L][hel]))) + "\n")

                                # write the phase
                                outFile.write(name + "_Phase\t" + str(sess.run(Interface.Phase(resonance.BSL_M[S][L][hel]))) + "\n")

        # print the B_LS couplings of the resonance decay
        for S in resonance.BSL_res.keys():
                for L in resonance.BSL_res[S].keys():
                        for hel in resonance.BSL_res[S][L].keys():

                                name = resonance.name + "__res_Bs" + str(S) + "l" + str(L) + "h" + str(hel)

                                # write the magnitude
                                outFile.write(name + "_Mag\t" + str(sess.run(Interface.Mag(resonance.BSL_res[S][L][hel]))) + "\n")

                                # write the phase
                                outFile.write(name + "_Phase\t" + str(sess.run(Interface.Phase(resonance.BSL_res[S][L][hel]))) + "\n")

        return

##############################

# Base class for all resonances
class Resonance:
	def __init__(self, name, label,
                     mass = 1., width = 0.1,
                     spin = 0, parity = +1,
                     aboveLmin = -1,
                     #
                     use_BLS_MagAndPhase = False,
                     use_BLS_RealAndImag = False,
                     #
                     couplings_dict_M = [],
                     couplings_dict_res = [],
                     #
                     resdecay_parity_conservation = False,
                     colour = 1, linewidth = 2, linestyle = 1, fillstyle = 0):

                # let's repeat the disclaimer:
                ########
                # The current implementation of the matrix elements
                # rely on the fact that you have a (spin 1/2) --> (spin 1/2) scalar scalar decay,
                # with d1 being the daughter particle with non null spin
                # If you try to use this exact implementation for a decay with different spin structures
                # do that at your own risk!
                ########

                logging.info("")
		logging.info("Resonance:: Creating resonance {}".format(name))

                # ask for non-zero spin, and |parity| = 1
		assert spin > 0 and abs(parity) is 1

		self.name = name
                self.label = label

                self.mass = mass
                self.width = width

                # quantum numbers of the state
		self.spin = spin
		self.parity = parity

                self.aboveLmin = aboveLmin

                self.resdecay_parity_conservation = resdecay_parity_conservation

                # set the convention of the B_LS couplings
                self.use_BLS_MagAndPhase = use_BLS_MagAndPhase
                self.use_BLS_RealAndImag = use_BLS_RealAndImag

                # some auxiliary variables
                self.couplings_dict_M = couplings_dict_M
                self.couplings_dict_res = couplings_dict_res

                # some style
                self.colour = colour
		self.linewidth = linewidth
		self.linestyle = linestyle
		self.fillstyle = fillstyle

                # used for the resonance shape
		self.m_ancestor = M.mass   # M mass
                self.d_parent = Const(1.5)  # barrier factor radius of the M
		self.d_res = Const(5.0)     # barrier factor radius of the resonance

                # B_LS couplings of the M and resonance decays
                self.BSL_M = {}
                self.BSL_res = {}

                # initialise the B_LS couplings and some more variables
                # this is specific for every species of resonance,
                # so you can find the method implementation directly there
                self.initialise_couplings()

        def set_couplings(self):

                #######
                # set the B_LS couplings
                #######

                if self.use_BLS_MagAndPhase :
                        logging.info("Resonance:: Setting the couplings using mag and phase convention")
                elif self.use_BLS_RealAndImag :
                        logging.info("Resonance:: Setting the couplings using real and imaginary parts convention")
                else :
                        logging.error("Resonance:: Error: the convention to use with the {} coupling is not correctly set.")
                        exit(1)

                # set the B_LS couplings for the M decay
                self.set_BSL_M()

                # set the B_LS couplings for the resonance decay
                self.set_BSL_res()

                #######
                # set the helicity amplitudes
                #######
                self.initialise_HelAmp_couplings()

        def set_BSL_M(self):

                #######
                # B_LS couplings of the M helicity amplitude
                #######

                # get all the possible B_LS couplings for the M decay
                S_values, L_values, self.BSL_M = get_all_possible_BSL(M.spin, self.spin, self.spectator.spin,
                                                                       helicities = None,  # no dependence of the couplings on the M helicities
                                                                       parity_conserved = False)

                logging.debug("B_LS = {}".format(self.BSL_M))
                logging.debug("Resonance:: set_BSL_M: S = {}".format(S_values))
                logging.debug("Resonance:: set_BSL_M: L = {}".format(L_values))

                Lmin = min(L_values)

                # angular momentum barrier: restrict the less likely values of L to the
                # (L_min, ..., L_min + aboveLmin) values
                if self.aboveLmin >= 0:
                        allowedL = [L for L in L_values if L <= (Lmin + self.aboveLmin)]

                logging.debug("Resonance:: set_BSL_M: L values of the M --> {} ## decay restricted to {}".format(self.name, allowedL))

                Smin = min(S_values)

                # get the number of couplings to be fitted,
                # and 'filter' the allowed B_LS couplings
                ncouplings = filter_allowed_couplings(self.BSL_M, allowedL)

                # print some infos
                logging.debug("Resonance:: set_BSL_M: allowedL = {}".format(allowedL))
                logging.debug("Resonance:: set_BSL_M: ncouplings = {}".format(ncouplings))

                logging.info("Resonance:: set_BSL_M: Lmin = {}, Smin = {}".format(Lmin, Smin))

                # set these damned M couplings
                self.BSL_M = self.set_BSL_couplings(self.BSL_M, self.couplings_dict_M, self.name + "__M")

                # logging.debug("Resonance::set_BSL_M: B_SL_M couplings = {}".format(sess.run(self.BSL_M)))

        def set_BSL_res(self):

                #######
                # B_LS couplings of the resonance helicity amplitude
                #######

                # get all the possible B_LS couplings for the resonance decay

                # L_values = orbital angular momenta between the resonance daughters
                S_values, L_values, self.BSL_res = get_all_possible_BSL(self.spin, self.a.spin, self.b.spin,
                                                                        helicities = self.helicities,  # dependence of the couplings on the res helicities!
                                                                        parity_conserved = self.resdecay_parity_conservation,
                                                                        P_A = self.parity, P_B = self.a.parity, P_C = self.b.parity)

                # L_parent = angular orbital momentum between the resonance and the spectator particle
                self.L_parent_min = min(GetLvalues_res_spectator(self.spin, self.spectator.spin))

                Lmin = min(L_values)

                # L_res = angular orbital momentum between the resonance daughters
                self.L_res_min = Lmin

                logging.debug("Resonance::set_BSL_res: S = {}".format(S_values))
                logging.debug("Resonance::set_BSL_res: L = {}".format(L_values))

                # angular momentum barrier: restrict the less likely values of L to the
                # (L_min, ..., L_min + aboveLmin) values
                if self.aboveLmin >= 0:
                        allowedL = [L for L in L_values if L <= (Lmin + self.aboveLmin)]

                logging.debug("Resonance::set_BSL_res: L values of the {} decay restricted to {}".format(self.name, allowedL))

                Smin = min(S_values)

                # get the number of couplings to be fitted,
                # and 'filter' the allowed B_LS couplings
                ncouplings = filter_allowed_couplings(self.BSL_res, allowedL)

                # print some infos
                logging.debug("Resonance::set_BSL_res: allowedL = {}".format(allowedL))
                logging.debug("Resonance::set_BSL_res: ncouplings = {}".format(ncouplings))

                logging.info("Resonance::set_BSL_res: Lmin = {}, Smin = {}".format(Lmin, Smin))

                # set these damned M couplings
                self.BSL_res = self.set_BSL_couplings(self.BSL_res, self.couplings_dict_res, self.name + "__res")

                logging.debug("Resonance::set_BSL_res: B_SL_res couplings = {}".format(self.BSL_res))

        # set the B_LS couplings
        def set_BSL_couplings(self, B_SL, couplings_dict, coupling_name):

                # set these damned B_LS couplings, looping over the S, L and helicity combinations
                for S in B_SL.keys():
                        for L in B_SL[S].keys():
                                for hel in B_SL[S][L].keys():

                                        name_parameter = coupling_name + "_Bs" + str(S) + "l" + str(L) + "h" + str(hel)

                                        if self.use_BLS_MagAndPhase :
                                                name_parameter_1 = name_parameter + "_Mag"
                                                name_parameter_2 = name_parameter + "_Phase"
                                        elif self.use_BLS_RealAndImag :
                                                name_parameter_1 = name_parameter + "_Real"
                                                name_parameter_2 = name_parameter + "_Imag"

                                                logging.debug("name_parameter_1 = {}, name_parameter_2 = {}".format(name_parameter_1, name_parameter_2))

                                                value_1 = -1
                                                value_2 = -1

                                                # search for the current coupling in the couplings of the decay, and set its components

                                                try:
                                                        value_1 = couplings_dict[name_parameter_1]
                                                        value_2 = couplings_dict[name_parameter_2]

                                                        # you might want to set some parameter (x) equal to another one (y):
                                                        # this is done if the value of x is a string,
                                                        # which is intended to be the name of y
                                                        if isinstance(value_1, basestring) :
		                                                value_1 = couplings_dict[value_1]

                                                        if isinstance(value_2, basestring) :
                                                                value_2 = couplings_dict[value_2]
                                                except :
                                                        logging.error(("Resonance::set_BSL_couplings: Error, the components of the {} coupling cannot be set."
                                                                       + " Check your config file or fit output").format(name_parameter))
                                                        logging.error("Those are the required components: {}, {}".format(name_parameter_1, name_parameter_2))
                                                        exit(1)

                                                if self.use_BLS_MagAndPhase :
                                                        B_SL[S][L][hel] = Interface.Polar(value_1, value_2)
                                                elif self.use_BLS_RealAndImag :
                                                        B_SL[S][L][hel] = Interface.Complex(value_1, value_2)
                                                else :
                                                        logging.error("Resonance::set_BSL_couplings: Error, no valid mode of using the BSL is selected")
                                                        exit(1)

                                                # print out some infos about the BLS components
                                                if type(value_1) is tf.Tensor :
                                                        logging.info("Resonance::set_BSL_couplings: Set {} = {}".format(name_parameter_1, sess.run(value_1)))
                                                elif type(value_1) is Optimisation.FitParameter :
                                                        logging.info("Resonance::set_BSL_couplings: Setting {} as fit parameter".format(name_parameter_1))
                                                else :
                                                        logging.error("Resonance::set_BSL_couplings: Error: unrecognised type of the {} component.".format(name_parameter_1))
                                                        exit(1)

                                                if type(value_2) is tf.Tensor :
                                                        logging.info("Resonance::set_BSL_couplings: Set {} = {}".format(name_parameter_2, sess.run(value_2)))
                                                elif type(value_2) is Optimisation.FitParameter :
                                                        logging.info("Resonance::set_BSL_couplings: Setting {} as fit parameter".format(name_parameter_2))
					        else :
                                                        logging.error("Resonance::set_BSL_couplings: Error: unrecognised type of the {} component.".format(name_parameter_2))
                                                        exit(1)

                # remove keys with empty values, just to be sure
                empty_d3eys = [k for k,v in B_SL.iteritems() if not v]

                for k in empty_d3eys:
                        logging.debug("Resonance::set_BSL_couplings: Found an empty key S = {}, removing it".format(k))
                        del B_SL[k]

                return B_SL

        def lineshape(self, msq):
                return CastComplex(Ones(msq))

##############################
#####  AResonance = d1 d2
##############################

# Base class for the A -> d1 d2 matrix element
# This version assumes a nonresonant lineshape. There are Breit Wigner and Flatte classes below,
# and it's rather trivial to extend and override the lineshape member function if you want more lineshapes
class AResonance(Resonance):

        def initialise_couplings(self):

                # remember the convention:
                # M --> (ma mb) m_spectator
                # this is later used for the resonant lineshapes
                self.a = d1
                self.b = d2
                self.spectator = d3

                self.species = "A-type"

                # the A can only take +-1/2 helicities, because of angular momentum conservation
                # (since the M decays into the A and a spinless particle)
                self.helicities = [-1, 1]

                # set the B_LS couplings and the helicity amplitudes
                self.set_couplings()

        # get the values of the amplitude variables
        def get_coordinates(self, phsp, datapoint):

                return phsp.ACoordinates(datapoint)

        # build all the helicity amplitudes
        def initialise_HelAmp_couplings(self):

                # for a A --> B C decay,
                # the helicity amplitudes depend on the helicities of B and C

                ##########
                # build all the helicity amplitudes for the M --> A d3 decay
		##########

                self.HelAmp_M = {}

		# here we have M --> A d3, then they only depend on hel_A
                # because of angular momentum conservation (A --> d1 d2),
		# hel_A can only be +-1/2

                # the M decay proceeds through weak decay,
                # then I don't have to consider parity conservation

                # loop over the A helicities
                for hel_A in self.helicities:

                        self.HelAmp_M[hel_A] = GetHelAmp(M.spin, "all",
                                                          0, 0,   # J_d3, hel_d3
                                                          self.spin, hel_A,
                                                          self.BSL_M)

                        logging.debug("AResonance::initialise_HelAmp_couplings: set HelAmp_M[hel_A = {}]".format(hel_A))

                ##########
                # build all the helicity amplitudes for the A --> d1 d2 decay
                ##########

                self.HelAmp_A = {}

                # here we have A --> d1 d2, then they only depend on hel_d1 (it can only be +-1/2)
                # Well this is not true: there is the dependence on the resonance helicity,
                # to take care of eventual polarisations

                self.HelAmp_A[-1] = {}
                self.HelAmp_A[+1] = {}

                for hel_A in self.helicities :
                        self.HelAmp_A[-1][hel_A] =  GetHelAmp(self.spin, hel_A,
                                                              0, 0,      # J_d2, hel_d2
                                                              d1.spin, -1,  # minhel_d1
                                                              self.BSL_res)

                        logging.debug("AResonance::initialise_HelAmp_couplings: initialised HelAmp_A[hel_d1 = -1][hel_A = {}]".format(hel_A))

                # now set the HelAmp with maximum hel_d1 (= +1)
                # and check for parity conservation
                if self.resdecay_parity_conservation :
                        parity_factor = self.parity * d2.parity * d1.parity * (-1)**(d2.spin + d1.spin - self.spin)

                        for hel_A in self.helicities :
                                self.HelAmp_A[+1][hel_A] = self.HelAmp_A[-1][hel_A] / parity_factor

                                logging.info("AResonance::initialise_HelAmp_couplings: power of parity!"
                                             + " Set HelAmp_A[hel_d1 = +1] = HelAmp_A[hel_d1 = -1] / {}".format(parity_factor))
                else :
                        # get the helicity amplitude in the 'standard' way

                        for hel_A in self.helicities :
                                self.HelAmp_A[+1][hel_A] = GetHelAmp(self.spin, hel_A,
                                                                     0, 0,      # J_d2, hel_d2
                                                                     d1.spin, +1,  # maxhel_d1
                                                                     B_SL = self.BSL_res)

                                logging.debug("AResonance::initialise_HelAmp_couplings: initialised HelAmp_A[hel_d1 = +1][hel_A = {}]".format(hel_A))

                return

        # compute the matrix element of the M --> A d3 decay
        # for a given set of M and A helicities
        def get_M_matrix_element(self, coordinates,
                                  hel_M, hel_A):

                # set the matrix element of the M decay,
                # which depends on the M and A helicities

                # get the amplitude variables that I need
                theta_M_A = coordinates["theta_M_A"]

                #remember that phid3min = 0. --> Polar(bla bla) = 1.
                M_M = (self.HelAmp_M[hel_A] *
                        CastComplex(Kinematics.Wignerd(theta_M_A, M.spin, hel_M, hel_A))  # hel_d3 = 0
                )

                return M_M

        # compute the matrix element of the A --> d1 d2 decay
        # for a given set of A and d1 helicities
        def get_res_matrix_element(self, coordinates,
                                   hel_A, hel_d1):

                # set the matrix element of the A decay,
                # which only depends on the A and d1 helicities

                # get the amplitude variables that I need
                theta_A = coordinates["theta_A"]
                phi_d1_A = coordinates["phi_d1_A"]
                m2_d1d2 = coordinates["m2_d1d2"]

                M_A = (self.HelAmp_A[hel_d1][hel_A] *
                       CastComplex(Kinematics.Wignerd(theta_A, self.spin, hel_A, hel_d1)) *  # hel_d2 = 0
                       Interface.Polar(1.0, hel_A * phi_d1_A) *
                       self.lineshape(m2_d1d2)  # line shape factor of the A resonance
                )

                return M_A

        # compute the complete matrix element for the M --> A d3, A --> d1 d2 decays,
        # for a given set of M and d1 helicities
	def get_complete_matrix_element(self, coordinates,
                                        hel_M, hel_d1):

                M_decay = 0.

                # loop over the A helicities
                for hel_A in self.helicities:

                        # get the M decay matrix
                        M_M = self.get_M_matrix_element(coordinates,
                                                          hel_M, hel_A)

                        # get the A decay matrix
                        M_A = self.get_res_matrix_element(coordinates,
                                                          hel_A, hel_d1)

                        # sum the M and A matrix elements
                        M_decay += M_M * M_A

                return M_decay

# Breit Wigner version of the AResonance class
class AResonanceBreitWigner(AResonance):
	def lineshape(self, msq):
		return Dynamics.BreitWignerLineShape(m2 = msq, m0 = self.mass, gamma0 = self.width,
                                                     m_parent = self.m_ancestor, ma = self.a.mass, mb = self.b.mass, mc = self.spectator.mass,
                                                     d_parent = self.d_parent, d_res = self.d_res,
                                                     L_parent = self.L_parent_min/2, L_res = self.L_res_min/2)

# Sub-threshold Breit Wigner version of the AResonance class
class AResonanceSubThresholdBreitWigner(AResonance):
	def lineshape(self, msq):
		return Dynamics.SubThresholdBreitWignerLineShape(m2 = msq, m0 = self.mass, gamma0 = self.width,
                                                                 m_parent = self.m_ancestor, ma = self.a.mass, mb = self.b.mass, mc = self.spectator.mass,
                                                                 d_parent = self.d_parent, d_res = self.d_res,
                                                                 L_parent = self.L_parent_min/2, L_res = self.L_res_min/2)

##############################
#####  BResonance = d2 d3
##############################

# Base class for the B -> d2 d3 matrix element
# This version assumes a nonresonant lineshape. There are Breit Wigner and Flatte classes below,
# and it's rather trivial to extend and override the lineshape member function if you want more lineshapes
class BResonance(Resonance):

        def initialise_couplings(self):

                # remember the convention:
                # M --> (ma mb) m_spectator
                # this is later used for the resonant lineshapes
                self.a = d2
                self.b = d3
                self.spectator = d1

                self.species = "B-type"

                # set the possible helicity values of the resonance
                self.helicities = range(-self.spin, self.spin + 1, 2)

                # set the B_LS couplings and the helicity amplitudes
                self.set_couplings()

        # get the values of the amplitude variables
        def get_coordinates(self, phsp, datapoint):

		return phsp.BCoordinates(datapoint)

        # build all the helicity amplitudes
        def initialise_HelAmp_couplings(self):

                # for a A --> B C decay,
		# the helicity amplitudes depend on the helicities of B and C

                ##########
		# build all the helicity amplitudes for the M --> A d3 decay
                ##########

                self.HelAmp_M = {}

                # here we have M --> B d1, then they both depend on hel_B and hel_d1

                # the M decay proceeds through weak decay,
                # then I don't have to consider parity conservation

                # loop over the B helicities
                for hel_B in self.helicities:

                        # if the (doubled) spin of the resonance is odd (i. e., the 'real' spin it's a fraction)
                        # then the null helicity is unphysical
                        if (self.spin % 2 == 1) and hel_B == 0:
                                continue

                        self.HelAmp_M[hel_B] = {}

                        logging.debug("BResonance::initialise_HelAmp_couplings: BSL_M = {}".format(self.BSL_M))

                        # loop over the d1 helicities
                        for hel_d1 in [-1, +1] :

                                self.HelAmp_M[hel_B][hel_d1] = GetHelAmp(M.spin, "all",
                                                                          d1.spin, hel_d1,
                                                                          self.spin, hel_B,
                                                                          B_SL = self.BSL_M)

                                logging.debug("BResonance::initialise_HelAmp_couplings: initialised HelAmp_M[hel_d1 = {}]".format(hel_d1))

                ##########
                # build all the helicity amplitudes for the B --> d2 d3 decay
                ##########

                # since B decays into two spinless particles,
                # there is only one helicity amplitude to compute
                # (i.e.: there is no dependence on the helicity of the daughter particles)
                # Well this is not true: there is the dependence on the resonance helicity,
                # to take care of eventual polarisations

                logging.debug("BResonance::initialise_HelAmp_couplings: BSL_res = {}".format(self.BSL_res))

                # get the helicity amplitude, for the resonance helicities

                self.HelAmp_B = {}

                for hel_B in self.helicities:

                        self.HelAmp_B[hel_B] = GetHelAmp(self.spin, hel_B,
                                                         0, 0,
                                                         0, 0,
                                                         self.BSL_res)

                        logging.debug("BResonance::initialise_HelAmp_couplings: initialised HelAmp_B[hel_B = {}]".format(hel_B))

        # compute the matrix element of the M --> B d1 decay
        # for a given set of M, B and d1 helicities
        def get_M_matrix_element(self, coordinates,
                                  hel_M, hel_B, hel_d1):

                # get the amplitude variables that I need
	        theta_M_B = coordinates["theta_M_B"]
                phi_d1_B = coordinates["phi_d1_B"]
                theta_d1_AB = coordinates["theta_d1_AB"]

                M_M = 0.

                # I have to perform this sum because the d1 quantisation axes are different
                # for the A and B decay chains
                for hel_d1_B in d1_helicities :

                        logging.debug("--> BResonance::amplitude: spin quantisation magic happening! hel_d1_B = {}".format(hel_d1_B))

                        M_M += (
                                # factor to align the d1 quantisation axes
                                CastComplex(Kinematics.Wignerd(theta_d1_AB, d1.spin, hel_d1_B, hel_d1)) *

                                self.HelAmp_M[hel_B][hel_d1_B] *
                                CastComplex(Kinematics.Wignerd(theta_M_B, M.spin, hel_M, hel_B - hel_d1_B)) *
                                Interface.Polar(1.0, hel_M * phi_d1_B)
                        )

                return M_M

        # compute the matrix element of the B --> d2 f3 decay
        # for a given B helicity value
        def get_res_matrix_element(self, coordinates,
                                   hel_B):

                # get the amplitude variables that I need
                theta_B = coordinates["theta_B"]
                phi_d2_B = coordinates["phi_d2_B"]
                m2_d2d3 = coordinates["m2_d2d3"]

                M_B = (self.HelAmp_B[hel_B] *
                       CastComplex(Kinematics.Wignerd(theta_B, self.spin, hel_B, 0)) *   # hel_d2 = 0, hel_d3 = 0
                       Interface.Polar(1.0, hel_B * phi_d2_B) *
                       self.lineshape(m2_d2d3) # line shape factor of the B resonance
                )

                return M_B

        # compute the complete matrix element for the M --> B d1, B --> d2 d3 decays,
        # for a given set of M and d1 helicities
        def get_complete_matrix_element(self, coordinates,
                                        hel_M, hel_d1):

                M_decay = 0.

                # loop over the B helicities
                for hel_B in self.helicities:

                        # if the (doubled) spin of the resonance is odd (i. e., the 'real' spin it's a fraction)
                        # then the null helicity is unphysical
			if (self.spin % 2 == 1) and hel_B == 0:
			        continue

                        # get the M decay matrix
                        M_M = self.get_M_matrix_element(coordinates,
                                                          hel_M, hel_B, hel_d1)

                        # get the B decay matrix
                        M_B = self.get_res_matrix_element(coordinates,
                                                          hel_B)

                        # sum the M and B matrix elements
                        M_decay += M_M * M_B

                return M_decay

# Breit Wigner version of the BResonance class
class BResonanceBreitWigner(BResonance):
        def lineshape(self, msq):
                return Dynamics.BreitWignerLineShape(m2 = msq, m0 = self.mass, gamma0 = self.width,
                                                     m_parent = self.m_ancestor, ma = self.a.mass, mb = self.b.mass, mc = self.spectator.mass,
                                                     d_parent = self.d_parent, d_res = self.d_res,
                                                     L_parent = self.L_parent_min/2, L_res = self.L_res_min/2)

# Sub-threshold Breit Wigner version of the BResonance class
class BResonanceSubThresholdBreitWigner(BResonance):
        def lineshape(self, msq):
                return Dynamics.SubThresholdBreitWignerLineShape(m2 = msq, m0 = self.mass, gamma0 = self.width,
                                                                 m_parent = self.m_ancestor, ma = self.a.mass, mb = self.b.mass, mc = self.spectator.mass,
                                                                 d_parent = self.d_parent, d_res = self.d_res,
                                                                 L_parent = self.L_parent_min/2, L_res = self.L_res_min/2)

##############################
#####  CResonance = d1 d3
##############################

# Base class for the B -> d2 d3 matrix element
# This version assumes a nonresonant lineshape. There are Breit Wigner and Flatte classes below,
# and it's rather trivial to extend and override the lineshape member function if you want more lineshapes
class CResonance(Resonance):

        def initialise_couplings(self):

                # remember the convention:
                # M --> (ma mb) mc
                # this is later used for the resonant lineshapes
                self.a = d1
                self.b = d3
                self.spectator = d2

                self.species = "C-type"

                # the C can only take +-1/2 helicities, because of angular momentum conservation
                # (since the M decays into the C and a spinless particle)
                self.helicities = [-1, 1]

                # set the B_LS couplings and the helicity amplitudes
                self.set_couplings()

        # get the values of the amplitude variables
        def get_coordinates(self, phsp, datapoint):

                return phsp.CCoordinates(datapoint)

        # build all the helicity amplitudes
        def initialise_HelAmp_couplings(self):

                # for a A --> B C decay,
                # the helicity amplitudes depend on the helicities of B and C

                ##########
                # build all the helicity amplitudes for the M --> C d2 decay
                ##########

                self.HelAmp_M = {}

                # here we have M --> C d2, then they only depend on hel_C
                # because of angular momentum conservation (C --> d1 d3),
                # hel_C can only be +-1/2

                # the M decay proceeds through weak decay,
                # then I don't have to consider parity conservation

                # loop over the C helicities
                for hel_C in self.helicities:

                        self.HelAmp_M[hel_C] = GetHelAmp(M.spin, "all",
                                                          0, 0,   # J_d2, hel_d2
                                                          self.spin, hel_C,
                                                          self.BSL_M)

                        logging.debug("CResonance::initialise_HelAmp_couplings: initialised HelAmp_M[hel_C = {}]".format(hel_C))

                ##########
                # build all the helicity amplitudes for the C --> d1 d3 decay
                ##########

                self.HelAmp_C = {}

                # here we have C --> d1 d3, then they only depend on hel_d1 (it can only be +-1/2)
                # Well this is not true: there is the dependence on the resonance helicity,
                # to take care of eventual polarisations

                self.HelAmp_C[-1] = {}
                self.HelAmp_C[+1] = {}

                for hel_C in self.helicities :
                        self.HelAmp_C[-1][hel_C] = GetHelAmp(self.spin, hel_C,
                                                             0, 0,         # J_d3, hel_d3
                                                             d1.spin, -1,  # minhel_d1
                                                             self.BSL_res)

                        logging.debug("CResonance::initialise_HelAmp_couplings: initialised HelAmp_C[hel_d1 = -1][hel_C = {}]".format(hel_C))

                # now set the HelAmp with maximum hel_d1 (= +1)
                # and check for parity conservation
                if self.resdecay_parity_conservation :
                        parity_factor = self.parity * d3.parity * d1.parity * (-1)**(d3.spin + d1.spin - self.spin)

                        for hel_C in self.helicities :
                                self.HelAmp_C[+1][hel_C] = self.HelAmp_C[-1][hel_C] / parity_factor

                                logging.info("CResonance::initialise_HelAmp_couplings: power of parity!"
                                             + " Set HelAmp_C[hel_d1 = +1, hel_C = {}]"
                                             + " = HelAmp_C[hel_d1 = -1, hel_C = {}] / {}".format(hel_C, hel_C, parity_factor))
                else :
                        # set the helicity amplitude in the 'standard' way

                        for hel_C in self.helicities :
                                self.HelAmp_C[+1] = GetHelAmp(self.spin, hel_C,
                                                              0, 0,         # J_d3, hel_d3
                                                              d1.spin, +1,  # maxhel_d1
                                                              self.BSL_res)

                                logging.debug("CResonance::initialise_HelAmp_couplings: initialised HelAmp_C[hel_d1 = +1][hel_C = {}]".format(hel_C))

                return

        # compute the matrix element of the M --> C d2 decay
        # for a given set of M and C helicities
        def get_M_matrix_element(self, coordinates,
                                  hel_M, hel_C):

                # get the amplitude variables that I need
                theta_M_C = coordinates["theta_M_C"]
                phi_d2_C = coordinates["phi_d2_C"]

                M_M = (self.HelAmp_M[hel_C] *
                        CastComplex(Kinematics.Wignerd(theta_M_C, M.spin, hel_M, hel_C)) *  # hel_d2 = 0
                        Interface.Polar(1.0, hel_M * phi_d2_C)
                        )

                return M_M

        # compute the matrix element of the C --> d1 d3 decay
        # for a given set of C and d1 helicities
        def get_res_matrix_element(self, coordinates,
				   hel_C, hel_d1):

                # get the amplitude variables that I need
                theta_C = coordinates["theta_C"]
                phi_d1_C = coordinates["phi_d1_C"]
                theta_d1_AC = coordinates["theta_d1_AC"]
                m2_d1d3 = coordinates["m2_d1d3"]

                M_C = 0.

                # I have to perform this sum because the d1 quantisation axes are different
                # for the A and C decay chains
                for hel_d1_C in d1_helicities :

                        logging.debug("--> CResonance::amplitude: spin quantisation magic happening! hel_d1_C = {}".format(hel_d1_C))

                        M_C += (
                                # factor to align the d1 quantisation axes
                                CastComplex(Kinematics.Wignerd(theta_d1_AC, d1.spin, hel_d1_C, hel_d1)) *

                                self.HelAmp_C[hel_d1_C][hel_C] *
                                CastComplex(Kinematics.Wignerd(theta_C, self.spin, hel_C, hel_d1_C)) *  # hel_d3 = 0
                                Interface.Polar(1.0, hel_C * phi_d1_C) *
                                self.lineshape(m2_d1d3) # line shape factor of the C resonance
                        )

                return M_C

        # compute the complete matrix element for the M --> C d2, C --> d1 d3 decays,
        # for a given set of M and d1 helicities
        def get_complete_matrix_element(self, coordinates,
                                        hel_M, hel_d1):

                M_decay = 0.

                # loop over the C helicities
                for hel_C in self.helicities:

                        # get the M decay matrix
                        M_M = self.get_M_matrix_element(coordinates,
                                                          hel_M, hel_C)

                        # get the C decay matrix
                        M_C = self.get_res_matrix_element(coordinates,
                                                          hel_C, hel_d1)

                        # sum the M and C matrix elements
                        M_decay += M_M * M_C

                return M_decay


# Breit Wigner version of the CResonance class
class CResonanceBreitWigner(CResonance):
        def lineshape(self, msq):
                return Dynamics.BreitWignerLineShape(m2 = msq, m0 = self.mass, gamma0 = self.width,
                                                     m_parent = self.m_ancestor, ma = self.a.mass, mb = self.b.mass, mc = self.spectator.mass,
                                                     d_parent = self.d_parent, d_res = self.d_res,
                                                     L_parent = self.L_parent_min/2, L_res = self.L_res_min/2)

# Sub-threshold Breit Wigner version of the CResonance class
class CResonanceSubThresholdBreitWigner(CResonance):
        def lineshape(self, msq):
                return Dynamics.SubThresholdBreitWignerLineShape(m2 = msq, m0 = self.mass, gamma0 = self.width,
                                                                 m_parent = self.m_ancestor, ma = self.a.mass, mb = self.b.mass, mc = self.spectator.mass,
                                                                 d_parent = self.d_parent, d_res = self.d_res,
                                                                 L_parent = self.L_parent_min/2, L_res = self.L_res_min/2)

# Flatte version of the CResonance class
# Assume the other channel is Xic pi, i.e. this is the Xic(2790)
# Construct just like BResonanceBreitWigner, but give width as a two-element list or tuple of tf tensors
class CResonanceFlatte(CResonance):
        def lineshape(self, msq):
                return Dynamics.FlatteLineShape(m2 = msq, m0 = self.mass, g1 = self.width[0], g2 = self.width[1]*self.width[0],
                                                m_parent = self.m_ancestor,
                                                ma1 = self.a.mass, mb1 = self.b.mass, ma2 = m_Xic, mb2 = K.mass,
                                                mc = self.spectator.mass,
                                                d_parent = self.d_parent, d_res = self.d_res,
                                                L_parent = self.L_parent_min/2, L_res = self.L_res_min/2)

##############################
#####  Non-resonant contribution
##############################

class NonResonant():

        def __init__(self,
                     nonres_size = Const(0.),
                     colour = 1, linewidth = 2, linestyle = 1, fillstyle = 0):

                logging.info("")
                logging.info("NonResonant:: Creating a non resonant contribution")

                # some style
                self.colour = colour
                self.linewidth = linewidth
                self.linestyle = linestyle
                self.fillstyle = fillstyle

                self.nonres_size = nonres_size

                name_parameter = "nonres_size"

                # I can set the non-resonant size from the (non)-resonance properties,
                # or from a set of constant values (i.e., the set of fitted values provided externally)
                # The latter is mainly for plotting

                # So, check if I have to override the property with the const value
                if fitted_values != [] :
                        try :
                                self.nonres_size = Const(fitted_values[name_parameter])
                        except :
                                logging.debug("all fine")

                # printout some infos
                if type(self.nonres_size) is tf.Tensor :
                        logging.info("NonResonant:: Set the non resonant size = {}".format(sess.run(self.nonres_size)))
                elif type(self.nonres_size) is Optimisation.FitParameter :
                        logging.info("NonResonant:: Setting the non resonant size as fit parameter")

        # save the resonant size to an output file
        def SaveNonResSize(self, outFile) :

                outFile.write("nonres_size\t" + str(sess.run(self.nonres_size)) + "\n")

                return

        def density(self):
                return Interface.Density(CastComplex(self.nonres_size))

##############################
#####  Matrix element
##############################

# extract the masses and angles from the datapoint
# and do the incoherent sum over initial/final state particle helicities and the coherent sum over resonances
def MatrixElement(phsp, datapoint, switches,
                  component_index = -1, resonances = [], nonres = None, eff_shape = None):

        # total density
        density = Const(0.)

        # add the density of a non-resonant contribution
        # its switch is the last element of the switches list
        if (component_index < 0 or component_index is (len(resonances))) and nonres != None :
                density += switches[-1] * nonres.density()

        # print the value of the parameters, only for debug
        #PrintParameters()

	# incoherent sum over the initial and final state particle helicities

        # sum over the M helicities
	for hel_M in M_helicities:

                # sum over the d1 helicities
		for hel_d1 in d1_helicities:

			amplitude = CastComplex(Const(0.))

                        # add all the amplitudes
                        if component_index < 0:

                                logging.debug("")
                                logging.debug("hel_M = {}, hel_d1 = {}".format(hel_M, hel_d1))

                                # loop over the resonances
                                for res, whatever in zip(resonances, switches) :
                                        logging.debug("MatrixElement:: Adding one amplitude {}".format(res.name))

                                        # get the values of the amplitude variables
                                        coordinates = res.get_coordinates(phsp, datapoint)

                                        # add the amplitude, already summed over the resonance helicities
                                        amplitude += CastComplex(whatever) * res.get_complete_matrix_element(coordinates, hel_M, hel_d1)

                        # speed up the graph building if you have only one component
                        elif component_index < len(resonances) :
                                # get the values of the amplitude variables
                                coordinates = resonances[component_index].get_coordinates(phsp, datapoint)

                                # get the amplitude for a single resonance, already summed over the resonance helicities
                                amplitude = CastComplex(switches[component_index]) * \
                                            resonances[component_index].get_complete_matrix_element(coordinates, hel_M, hel_d1)

			density += Interface.Density(amplitude)

        # add the efficiency contribution to the PDF,
        # over the Dalitz plot
        if eff_shape != None :
                density *= eff_shape.shape(tf.stack([phase_space.cosThetaM(datapoint),
                                                     (phase_space.Md1d2(datapoint))**2],
                                                    axis = 1))
                #density *= eff_shape.shape(tf.stack([phase_space.cosThetaM(datapoint),
                #                                     phase_space.phid1(datapoint),
                #                                    (phase_space.Md1d2(datapoint))**2],
                #                                    axis = 1))

	return density

#########

# compute the interference amplitude between two states
def MatrixElement_interference(phsp, datapoint, component_indeces = [], resonances = []):

        # total density
        density = CastComplex(0.)

        # incoherent sum over the initial and final state particle helicities

        # sum over the M helicities
        for hel_M in M_helicities:

                # sum over the d1 helicities
                for hel_d1 in d1_helicities:

                        # list of all interfering amplitudes
                        amplitudes = []

                        # amplitude of a single resonance
                        single_amplitude = CastComplex(Const(0.))

                        # loop over the indeces of the interfering indeces
                        for index in component_indeces :

                                # get the values of the amplitude variables
                                coordinates = resonances[index].get_coordinates(phsp, datapoint)

                                # get the amplitude of a single resonance, already summed over the resonance helicities
                                single_amplitude = resonances[index].get_complete_matrix_element(coordinates, hel_M, hel_d1)

                                amplitudes.append(single_amplitude)

                        # ok, now is getting fishy:
                        # I assume we only have interference between two states
                        # just to be sure, check it
                        if len(amplitudes) != 2:
                                logging.error("MatrixElement_interference:: Error, the interference amplitudes are not 2."
                                              + " len(amplitudes) = {}.".format(len(amplitudes)))
                                exit(1)

                        # multiply the first amplitude by the complex conjugate of the second one
                        density += tf.multiply(amplitudes[0], Interface.Conjugate(amplitudes[1]))

        return density

#########

# compute the matrix elements to get the uncertainty on the fit fraction
# here is getting really, really... well...

# this is only the numerator (or better, the density of the numerator) of the uncertainty:
# look at the note for all the details
# the denominator is sqrt(N_events), and they are assembled somewhere else

def FitUncertainty_numerator(phsp, datapoint, resonance):

        # get the values of the amplitude variables
        coordinates = resonance.get_coordinates(phsp, datapoint)

        # the uncertainty of the fit fractions essentialy depends on two factor:
        # one constituted by some combination of the matrix elements,
        # the other by the B_LS couplings

        ##########
        # matrix elements contributions
        ##########

        matrix_contributions = 0.
        sum_MM_cdot_Mres = 0.

        # loop over the M helicities
        for hel_M in M_helicities:

                # loop over the d1 helicities
                for hel_d1 in d1_helicities:

                        # loop over the resonance helicities
                        for hel_res in resonance.helicities :

                                # set the M and resonance decay matrices, depending on the species of the resonance
                                # this is ugly I know, but... whatever
                                if resonance.species == "B-type" :
                                        M_M = resonance.get_M_matrix_element(coordinates, hel_M, hel_res, hel_d1)
                                        M_res = resonance.get_res_matrix_element(coordinates, hel_res)
                                else :
                                        M_M = resonance.get_M_matrix_element(coordinates, hel_M, hel_res)
                                        M_res = resonance.get_res_matrix_element(coordinates, hel_res, hel_d1)

                                sum_MM_cdot_Mres += tf.abs(M_M * M_res)**2


                        # multiply the previous sum by |2*M_decay|^2,
                        # where M_decay is the complete matrix element of the M and resonance decays,
                        # summed already over the resonance helicities
                        matrix_contributions += tf.abs(2 * resonace.get_complete_matrix_element(hel_M, hel_d1))**2 * sum_MM_cdot_Mres

        ##########
        # B_LS contributions
        ##########

        # B_LS couplings of the M decay
        sum_delta_BSL_M = 0.
        sum_BSL_M = 0.

        for BSL_M in resonance.BSL_M.B_SL :
                sum_BSL_M += BSL_M.fitted_value
                sum_delta_BSL_M += (BSL_M.error)**2

        sum_BSL_M = sum_BSL_M**2

        # B_LS couplings of the resonance decay
        sum_delta_BSL_res = 0.
        sum_BSL_res = 0.

        for BSL_res in resonance.BSL_M.B_SL :
                sum_BSL_res += BSL_res.fitted_value
                sum_delta_BSL_res += (BSL_res.error)**2

        sum_BSL_M = sum_BSL_res**2

        # total B_LS contribution to the fit fraction uncertainty
        B_LS_contributions = (sum_delta_BSL_M/sum_BSL_M) + (sum_delta_BSL_res/sum_BSL_res)

        return Interface.Sqrt(matrix_contributions * B_LS_contributions)

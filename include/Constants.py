
# Alessio Piucci
# 17/11/2017
# Python module containing some constants... and settings

import pypdt

import sys

#import the local libraries
sys.path.append("../amplitude_fits/")

from Constants import *
import Optimisation
from Interface import Const

# helicities of the (pseudo)scalars, just to avoid to put hard-coded 0 in the code
#hel_d2 = 0
#hel_d3 = 0

# some colours used by the plotting macros
kRed = 2
kYellow = 5
kBlack = 1
kBlue = 4
kGreen = 416
kMagenta = 616
kGray = 920
kBrown = 28
kWhite = 0
kAzure = 860
kPink = 900
kCyan = 432
kSpring = 820
KOrange = 800
kViolet = 880
kTeal = 840

##############

# class to store PDG infos of resonances
class PDGPart():
    def __init__(self, name, label,
                 mass, width, spin, parity):

        self.name = name
        self.label = label
        self.mass = mass
        self.width = width
        self.spin = spin
        self.parity = parity

##############

# remember that the massese are in GeV/c^2,
# and the spin values are doubled!

Lb = PDGPart(name = "Lb", label = "#Lambda_{b}^{0}",
             mass = pypdt.PDT()[5122].mass, width = Const(0.205),
             spin = 1, parity = +1)

Lc = PDGPart(name = "Lc", label = "#Lambda_{c}^{+}",
             mass = pypdt.PDT()[4122].mass, width = Const(0.205),
             spin = 1, parity = +1)

D0 = PDGPart(name = "D0", label = "D^{0}",
             mass = pypdt.PDT()[421].mass, width = Const(0.205),
             spin = 0, parity = -1)

K = PDGPart(name = "K", label = "K^{-}",
            mass = pypdt.PDT()[321].mass, width = Const(0.00001),
            spin = 0, parity = -1)

proton = PDGPart(name="p", label = "p",
                 mass=pypdt.PDT()[2212].mass, width=Const(0.00001),
                 spin=1, parity=+1)

pion = PDGPart(name="pi", label = "pi^{-}",
               mass=pypdt.PDT()[211].mass,width=Const(0.0001),
               spin=0, parity=-1)



# to specify our particular decay within the PhaseSpace and MatrixElement modules,
# define which are the M --> d1 d2 d3 particles
# If you have a different decay (but with same spin compositions),
# you only have to specify different M, d1, d2 and 3... --> IN PRINCIPLE <--
M = Lc
d1 = proton
d2 = pion
d3 = K

######

# list of all possible resonances which might contribute

Kst890 = PDGPart(name="Kst890",label="K^{*}",
                 mass=Const(pypdt.PDT()[313].mass),
                 width=Const(pypdt.PDT()[313].width),
                 spin=2,parity=-1)


Lambda1520 = PDGPart(name="L1520",label="\Lambda^{*}(1520)",
                 mass=Const(1.5195),
                 width=Const(0.015),
                 spin=3,parity=-1)

Delta1232 = PDGPart(name="D1232",label="\Delta^{++}(1232)",
                    mass=Const(pypdt.PDT()[2224].mass),
                    width=Const(pypdt.PDT()[2224].width),
                    spin=3,parity=1)

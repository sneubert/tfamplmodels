# copy the config file to the work dir
template = "../config/resonances_plotLc2ppiK.py"
config_new = "../config/resonances_plotLc2ppiK_TEMP.py"
from shutil import copyfile
import os

copyfile(template, config_new)


# open the file containing the fit results
file = open("fitResult_params_cartesian.info", "r")

with open(config_new,"r") as f:
    s=f.read()

with open(config_new,"w") as outf :

# loop over the lines of the file containing the fit results
    for line in file : 
      line = line.rstrip(os.linesep)
      param_name = line.split("\t")[0]
      param_value = line.split("\t")[1]

      print param_name + " : " + str(param_value)

      # put the parameter value in the config file
      s=s.replace(param_name + "_VALUE", param_value)
    outf.write(s)

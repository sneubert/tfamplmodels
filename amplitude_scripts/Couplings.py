
import tensorflow as tf
import pypdt
import ROOT
import sys
import math

#import the local libraries
sys.path.append("../externals/TensorFlowAnalysis/TensorFlowAnalysis/")

import Kinematics
import Dynamics

import Optimisation
import Interface
from Interface import Const, Complex, CastComplex

sys.path.append("../include/")
sys.path.append("../config/")

from Constants import *

# import the Python settings
from python_settings import *

######

# some global variables
step_size = 0.01  #0.01

sess = None

######

# save all the BLS couplings to an output file,
# in cartesian coordinates
def SaveBSL_cartesian(resonance, outFile) :
        
        # print the B_LS couplings of the Lb decay
        for S in resonance.BSL_Lb.B_SL.keys():
                for L in resonance.BSL_Lb.B_SL[S].keys():
                        for hel	in resonance.BSL_Lb.B_SL[S][L].keys():

                                name = resonance.name + "__Lb_Bs" + str(S) + "l" + str(L) + "h" + str(hel)
                                
                                # write the magnitude
                                outFile.write(name + "_Real\t" + str(sess.run(Interface.Real(resonance.BSL_Lb.B_SL[S][L][hel]))) + "\n")
                                
                                # write the phase
                                outFile.write(name + "_Imag\t" + str(sess.run(Interface.Imaginary(resonance.BSL_Lb.B_SL[S][L][hel]))) + "\n")
                        
        # print the B_LS couplings of the resonance decay
        for S in resonance.BSL_res.B_SL.keys():
                for L in resonance.BSL_res.B_SL[S].keys():
                        for hel	in resonance.BSL_res.B_SL[S][L].keys():

                                name = resonance.name + "__res_Bs" + str(S) + "l" + str(L) + "h" + str(hel)
                                
                                # write the magnitude
                                outFile.write(name + "_Real\t" + str(sess.run(Interface.Real(resonance.BSL_res.B_SL[S][L][hel]))) + "\n")
                                
                                # write the phase
                                outFile.write(name + "_Imag\t" + str(sess.run(Interface.Imaginary(resonance.BSL_res.B_SL[S][L][hel]))) + "\n")
                        
        return

# save all the BLS couplings to an output file,
# in polar coordinates
def SaveBSL_polar(resonance, outFile) :

	# print the B_LS couplings of the Lb decay
        for S in resonance.BSL_Lb.B_SL.keys():
                for L in resonance.BSL_Lb.B_SL[S].keys():
                        for hel	in resonance.BSL_Lb.B_SL[S][L].keys():
                                
                                name = resonance.name + "__Lb_Bs" + str(S) + "l" + str(L) + "h" + str(hel)
                                
                                # write the magnitude
                                outFile.write(name + "_Mag\t" + str(sess.run(Interface.Mag(resonance.BSL_Lb.B_SL[S][L][hel]))) + "\n")
                                
                                # write the phase
                                outFile.write(name + "_Phase\t" + str(sess.run(Interface.Phase(resonance.BSL_Lb.B_SL[S][L][hel]))) + "\n")

	# print the B_LS couplings of the resonance decay
        for S in resonance.BSL_res.B_SL.keys():
		for L in resonance.BSL_res.B_SL[S].keys():
                        for hel in resonance.BSL_res.B_SL[S][L].keys():
                                
                                name = resonance.name + "__res_Bs" + str(S) + "l" + str(L) + "h" + str(hel)
                                
                                # write the magnitude
                                outFile.write(name + "_Mag\t" + str(sess.run(Interface.Mag(resonance.BSL_res.B_SL[S][L][hel]))) + "\n")
                                
                                # write the phase
                                outFile.write(name + "_Phase\t" + str(sess.run(Interface.Phase(resonance.BSL_res.B_SL[S][L][hel]))) + "\n")

        return

###########################

# base class for the B_LS couplings
class Couplings:
        def __init__(self, coupling_name,
                     use_BLS_MagAndPhase = False,
                     use_BLS_RealAndImag = False) :

                # B_LS couplings
                self.B_SL = {}

                self.coupling_name = coupling_name

                # set the convention that you want to use
                self.use_BLS_MagAndPhase = use_BLS_MagAndPhase
                self.use_BLS_RealAndImag = use_BLS_RealAndImag
                
                if self.use_BLS_MagAndPhase :
                        logging.info("Couplings:: Setting the {} coupling using mag and phase convention".format(coupling_name))
                elif self.use_BLS_RealAndImag :
                        logging.info("Couplings:: Setting the {} coupling using real and imaginary parts convention".format(coupling_name))
                else :
                        logging.error("Couplings:: Error: the convention to use with the {} coupling is not correctly set.".format(coupling_name))
                        exit(0)
                
        ################

        # filter the allowed couplings, and returns their number
        def select_allowed_couplings(self, allowedL):

                num_couplings = 0

                # loop over the S, L and helicity values
                for S in self.B_SL.keys():
                        for L in self.B_SL[S].keys():
                                
                                # only accept the B_SL couplings with the allowed L values
                                # delete the the ones not allowed
                                if not L in allowedL:
                                        logging.debug("Couplings::select_allowed_couplings: Deleting B_SL[L = {}, S = {}]".format(S, L))
                                        
                                        del self.B_SL[S][L]
                                        
                                        continue
                                
                                num_couplings += 1
                                
                return num_couplings
        
        ################
        
        # set all the B_LS couplings
        def set_BSL_couplings(self, BSLh, const_values = []):
                
                # set these damned B_LS couplings, looping over the S, L and helicity combinations
                for S in self.B_SL.keys():
                        for L in self.B_SL[S].keys():
                                for hel in self.B_SL[S][L].keys():
                                        name_parameter = self.coupling_name + "_Bs" + str(S) + "l" + str(L) + "h" + str(hel)
                                        
                                        if self.use_BLS_MagAndPhase :
                                                name_parameter_1 = name_parameter + "_Mag"
                                                name_parameter_2 = name_parameter + "_Phase"
                                        elif self.use_BLS_RealAndImag :
                                                name_parameter_1 = name_parameter + "_Real"
                                                name_parameter_2 = name_parameter + "_Imag"

                                        logging.debug("name_parameter_1 = {}, name_parameter_2 = {}".format(name_parameter_1, name_parameter_2))
                                        
                                        value_1 = -1
                                        value_2 = -1
                                
                                        # search for the current coupling in the couplings of the decay, and set its components
                                        
                                        # set the couplings to the values specified in the resonance properties
                                        # or override them with the values in const_values
                                        
                                        try :
                                                # set the values from the constant values
                                                value_1 = Const(const_values[name_parameter_1])
                                                value_2 = Const(const_values[name_parameter_2])
                                                
                                                logging.info("")
                                                logging.info("Couplings::set_BSL_couplings: Overriding the default couplings with the external ones.")
                                                
                                        except :
                                                # set the values from the resonance properties
                                                try:
                                                        value_1 = BSLh[name_parameter_1]                                                
                                                        value_2 = BSLh[name_parameter_2]
                                                
                                                        # you might want to set some parameter (x) equal to another one (y):
                                                        # this is done if the value of x is a string,
                                                        # which is intended to be the name of y
                                                        if isinstance(value_1, basestring) :
                                                                value_1 = BSLh[value_1]
                                                                
                                                        if isinstance(value_2, basestring) :
                                                                value_2 = BSLh[value_2]
                                                                
                                                except :
                                                        logging.error(("Couplings::set_BSL_couplings: Error, the components of the {} coupling cannot be set."
                                                                       + " Check your config file or fit output").format(name_parameter))
                                                        logging.error("Those are the required components: {}, {}".format(name_parameter_1, name_parameter_2))
                                                        exit(0)
                                
                                        if self.use_BLS_MagAndPhase :
                                                self.B_SL[S][L][hel] = Interface.Polar(value_1, value_2)
                                        elif self.use_BLS_RealAndImag :
                                                self.B_SL[S][L][hel] = Interface.Complex(value_1, value_2)
                                        else :
                                                logging.error("Couplings::set_BSL_couplings: Error, no valid mode of using the BSL is selected")
                                                exit(0)
                                                
                                        # print out some infos about the BLS components
                                        if type(value_1) is tf.Tensor :
                                                logging.info("Couplings::set_BSL_couplings: Set {} = {}".format(name_parameter_1, sess.run(value_1)))
                                        elif type(value_1) is Optimisation.FitParameter :
                                                logging.info("Couplings::set_BSL_couplings: Setting {} as fit parameter".format(name_parameter_1))
                                                
                                        if type(value_2) is tf.Tensor :
                                                logging.info("Couplings::set_BSL_couplings: Set {} = {}".format(name_parameter_2, sess.run(value_2)))
                                        elif type(value_2) is Optimisation.FitParameter :
                                                logging.info("Couplings::set_BSL_couplings: Setting {} as fit parameter".format(name_parameter_2))

                                        
                # my job is finished here, let's go home
                return
        

        # print the B_LS couplings, for debug purposes
        def print_BSL(self) :

                # loop over the L and S values
                for S in self.B_SL.keys():
                        for L in self.B_SL[S].keys():
                                for hel in self.B_SL[S][L].keys():
                                        # build the coupling name
                                        name_parameter = self.coupling_name + "_Bs" + str(S) + "l" + str(L) + "h" + str(hel)
                                        
                                        # print the values
                                        logging.info("Couplings::print_BSL: {} = {}".format(name_parameter, sess.run(self.B_SL[S][L][hel])))
                                
                return




        

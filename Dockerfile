FROM rootproject/root-ubuntu16

MAINTAINER sneubert@cern.ch

USER root

# need kerberos to access eos
ADD krb5.conf /etc/krb5.conf
RUN apt-get -y update \
    && apt-get -y install krb5-user


# adding the analysis code to the container
WORKDIR /code
ADD . /code
RUN chmod +x run.sh

CMD ["./run.sh"]


import sys
import math

#import the local libraries
from Constants import *

################

# variables

# i = variable
# [i][0] = name of the variable as encoded in the PhaseSpace and MatrixElement modules:
#          please DO NOT tough them, or most likely you will get a crash as result
# [i][1] = name of the variable as it is in the input sample,
#          it will be used to name the output files and plots
# [i][2], [i][3] = min and max of the x axis
# [i][4] = title of the plot
# [i][5] = units

# main variables, which describe the decay
variables = [
    ["m_d1d2", "Mppi",
     proton.mass + pion.mass, Lc.mass - K.mass,                 # axis range
     "#it{m}(#it{p}^{#plus}#it{#pi}^{#plus})",  # title
     "GeV/#it{c}^{2}"],                                   # units

    ["cosThetaM", "cosThetaLc",
     -1, +1,                          # axis range
     "cos#it{#theta_{#Lambda_{c}}}",  # title
     ""],                             # units

    ["cosThetaA", "cosThetaA",
     -1, +1,                # axis range
     "cos#it{#theta_{A}}",  # title
     ""],                   # units

    ["phid1", "phip",
     -math.pi, math.pi,          # axis range
     "#it{#phi_{p}}",  # title
     ""]                         # units
]

# additional variables to plot
other_variables = [
    ["m_d2d3", "MpiK",
     pion.mass + K.mass + 0.5, Lc.mass - proton.mass - 0.5,     # axis range
     "#it{m}(#it{{#pi}^{+}}#it{K}^{#minus})",           # title
     "GeV/#it{c}^{2}"],                                   # units

    ["m_d1d3", "MpK",
     proton.mass + K.mass + 0.5, Lc.mass - pion.mass - 0.5,   # axis range
     "#it{m}(#it{p}^{+}#it{K}^{#minus})",     # title
     "GeV/#it{c}^{2}"],                                 # units

    ["cosThetaB", "cosThetaB",
     -1, +1,                # axis range
     "cos#it{#theta_{B}}",  # title
     ""],                   # units

    ["cosThetaC", "cosThetaC",
     -1, +1,                # axis range
     "cos#it{#theta_{C}}",  # title
     ""],                   # units

    ["phid1_B", "phip_B",
     -math.pi, math.pi,              # axis range
     "#it{#phi_{p,B}}",    # title
     ""],                            # units

    ["phid2_B", "phipi_B",
     -math.pi, math.pi,              # axis range
     "#it{#phi_{{#pi}^{+}_{B}}}",  # title
     ""],                            # units

    ["phid2_C", "phipi_C",
     -math.pi, math.pi,              # axis range
     "#it{#phi_{{#pi}^{+}_{C}}}",  # title
     ""],                            # units

    ["phid1_C", "phip_C",
     -math.pi, math.pi,              # axis range
     "#it{#phi_{p,C}}",    # title
     ""],                            # units

]

############

# all the kinematic variables of the particles
variables_kin = [
    ["M_PX", "Lc_PX",
     -100000., 100000.,
     "#it{p_{#Lambda_{c}}}",
     "MeV/c"
    ],

    ["M_PY", "Lc_PY",
     -100000., 100000.,
     "#it{p_{#Lambda_{c}}}",
     "MeV/c"
    ],

    ["M_PZ", "Lc_PZ",
     0., 100000.,
     "#it{p_{#Lambda_{c}}}",
     "MeV/c"
    ],

    ["M_E", "Lc_E",
     0., 100000.,
     "#it{E_{#Lambda_{c}}}",
     "MeV"
    ],

    ["d1_PX", "p_PX",
     -100000., 100000.,
     "#it{p_{#p}}",
     "MeV/c"
    ],

    ["d1_PY", "p_PY",
     -100000., 100000.,
     "#it{p_{#p}}",
     "MeV/c"
    ],

    ["d1_PZ", "p_PZ",
     0., 100000.,
     "#it{p_{#p}}",
     "MeV/c"
    ],

    ["d1_E", "p_E",
     0., 100000.,
     "#it{E_{#p}}",
     "MeV"
    ],

    ["d2_PX", "pi_PX",
     -100000., 100000.,
     "#it{p_{#pi^{+}}}",
     "MeV/c"
    ],

    ["d2_PY", "pi_PY",
     -100000., 100000.,
     "#it{p_{#pi^{+}}}",
     "MeV/c"
    ],

    ["d2_PZ", "pi_PZ",
     0., 100000.,
     "#it{p_{#pi^{+}}}",
     "MeV/c"
    ],

    ["d2_E", "pi_E",
     0., 100000.,
     "#it{E_{#pi^{+}}}",
     "MeV"
    ],

    ["d3_PX", "K_PX",
     -100000., 100000.,
     "#it{p_{K^{-}}}",
     "MeV/c"
    ],

    ["d3_PY", "K_PY",
     -100000., 100000.,
     "#it{p_{K^{-}}}",
     "MeV/c"
    ],

    ["d3_PZ", "K_PZ",
     0., 100000.,
     "#it{p_{K^{-}}}",
     "MeV/c"
    ],

    ["d3_E", "K_E",
     0., 100000.,
     "#it{E_{K^{-}}}",
     "MeV"
    ]
]


import sys
import math

#import the local libraries
from Constants import *

################

# variables

# i = variable
# [i][0] = name of the variable as encoded in the PhaseSpace and MatrixElement modules:
#          please DO NOT tough them, or most likely you will get a crash as result
# [i][1] = name of the variable as it is in the input sample,
#          it will be used to name the output files and plots
# [i][2], [i][3] = min and max of the x axis
# [i][4] = title of the plot
# [i][5] = units

# main variables, which describe the decay
variables = [
    ["m_d1d2", "MLcD0",
     Lc.mass + D0.mass, Lb.mass - K.mass,                 # axis range
     "#it{m}(#it{#Lambda}^{#plus}_{c}#it{#bar{D}^{0}})",  # title
     "GeV/#it{c}^{2}"],                                   # units

    ["cosThetaM", "cosThetaLb",
     -1, +1,                          # axis range
     "cos#it{#theta_{#Lambda_{b}}}",  # title
     ""],                             # units

    ["cosThetaA", "cosThetaA",
     -1, +1,                # axis range
     "cos#it{#theta_{A}}",  # title
     ""],                   # units

    ["phid1", "phiLc",
     -math.pi, math.pi,          # axis range
     "#it{#phi_{#Lambda_{c}}}",  # title
     ""]                         # units
]

# additional variables to plot
other_variables = [
    ["m_d2d3", "MD0K",
     D0.mass + K.mass + 0.5, Lb.mass - Lc.mass - 0.5,     # axis range
     "#it{m}(#it{#bar{D}^{0}}#it{K}^{#minus})",           # title
     "GeV/#it{c}^{2}"],                                   # units
    
    ["m_d2d3", "MLcK",
     Lc.mass + K.mass + 0.5, Lb.mass - D0.mass - 0.5,   # axis range
     "#it{m}(#it{#Lambda}^{+}_{c}#it{K}^{#minus})",     # title
     "GeV/#it{c}^{2}"],                                 # units

    ["cosThetaB", "cosThetaB",
     -1, +1,                # axis range
     "cos#it{#theta_{B}}",  # title
     ""],                   # units

    ["cosThetaC", "cosThetaC",
     -1, +1,                # axis range
     "cos#it{#theta_{C}}",  # title
     ""],                   # units

    ["phid1_B", "phiLc_B",
     -math.pi, math.pi,              # axis range
     "#it{#phi_{#Lambda_{c},B}}",    # title
     ""],                            # units

    ["phid2_B", "phiD0_B",
     -math.pi, math.pi,              # axis range
     "#it{#phi_{#bar{D}^{0}_{B}}}",  # title
     ""],                            # units

    ["phid2_C", "phiD0_C",
     -math.pi, math.pi,              # axis range
     "#it{#phi_{#bar{D}^{0}_{C}}}",  # title
     ""],                            # units

    ["phid1_C", "phiLc_C",
     -math.pi, math.pi,              # axis range
     "#it{#phi_{#Lambda_{c},C}}",    # title
     ""],                            # units
    
]

############

# all the kinematic variables of the particles
variables_kin = [
    ["M_PX", "Lb_PX",
     -100000., 100000.,
     "#it{p_{#Lambda_{b}}}",
     "MeV/c"
    ],

    ["M_PY", "Lb_PY",
     -100000., 100000.,
     "#it{p_{#Lambda_{b}}}",
     "MeV/c"
    ],

    ["M_PZ", "Lb_PZ",
     0., 100000.,
     "#it{p_{#Lambda_{b}}}",
     "MeV/c"
    ],

    ["M_E", "Lb_E",
     0., 100000.,
     "#it{E_{#Lambda_{b}}}",
     "MeV"
    ],

    ["d1_PX", "Lc_PX",
     -100000., 100000.,
     "#it{p_{#Lambda_{c}}}",
     "MeV/c"
    ],

    ["d1_PY", "Lc_PY",
     -100000., 100000.,
     "#it{p_{#Lambda_{c}}}",
     "MeV/c"
    ],

    ["d1_PZ", "Lc_PZ",
     0., 100000.,
     "#it{p_{#Lambda_{c}}}",
     "MeV/c"
    ],

    ["d1_E", "Lc_E",
     0., 100000.,
     "#it{E_{#Lambda_{c}}}",
     "MeV"
    ],

    ["d2_PX", "D0_PX",
     -100000., 100000.,
     "#it{p_{D^{0}}}",
     "MeV/c"
    ],

    ["d2_PY", "D0_PY",
     -100000., 100000.,
     "#it{p_{D^{0}}}",
     "MeV/c"
    ],

    ["d2_PZ", "D0_PZ",
     0., 100000.,
     "#it{p_{D^{0}}}",
     "MeV/c"
    ],

    ["d2_E", "D0_E",
     0., 100000.,
     "#it{E_{D^{0}}}",
     "MeV"
    ],

    ["d3_PX", "K_PX",
     -100000., 100000.,
     "#it{p_{K^{-}}}",
     "MeV/c"
    ],

    ["d3_PY", "K_PY",
     -100000., 100000.,
     "#it{p_{K^{-}}}",
     "MeV/c"
    ],

    ["d3_PZ", "K_PZ",
     0., 100000.,
     "#it{p_{K^{-}}}",
     "MeV/c"
    ],

    ["d3_E", "K_E",
     0., 100000.,
     "#it{E_{K^{-}}}",
     "MeV"
    ]
]


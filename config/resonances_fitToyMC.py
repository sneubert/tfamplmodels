
import sys

#import the local libraries
from Constants import *
import MatrixElement
import Optimisation
from Interface import Const
from Optimisation import FitParameter

# label of the data legend
data_legend = "toy data"

# non-resonant component
#non_res = MatrixElement.NonResonant(nonres_size = FitParameter["nonres_size", 5., 0., 10., 0.1],
#                                    colour = kYellow, linestyle = 7, fillstyle = 3954)
non_res = MatrixElement.NonResonant(nonres_size = Const(0.))

# list of Lc D0 resonances
A = [
    # Lb --> Pc K
    #MatrixElement.AResonanceBreitWigner(Pc_4380.name, Pc_4380.label,
    #                                    fix_mass = Pc_4380.mass, fix_width = Pc_4380.width,
    #                                    spin = Pc_4380.spin, parity = Pc_4380.parity, aboveLmin = 0,
    #                                    use_BLS_RealAndImag = True,
    #                                    fitparams_BLS_Lb_real = [0.1, -2., 2.], fitparams_BLS_Lb_imag = [0., -2., 2.],
    #                                    fixBLS_res_toone = True,
    #                                    colour = kAzure, linestyle = 7, fillstyle = 3954),
    
    # Lb --> Pc K
    #MatrixElement.AResonanceBreitWigner(name = Pc_4450.name, label = Pc_4450.label,
    #                                    fix_mass = Pc_4450.mass, fix_width = Pc_4450.width,
    #                                    spin = Pc_4450.spin, parity = Pc_4450.parity, aboveLmin = 0,
    #                                    use_BLS_RealAndImag = True,
    #                                    fitparams_BLS_Lb_real = [0.1, -2., 2.], fitparams_BLS_Lb_imag = [0., -2., 2.],
    #                                    fixBLS_res_toone = True,
    #                                    colour = kMagenta, linestyle = 7, fillstyle = 3954),
]

# list of D0 K resonances
B = [
    # Lb --> Lc Ds1*(2700)
    MatrixElement.BResonanceBreitWigner(name = Ds1_2700.name, label = Ds1_2700.label,
                                        mass = Ds1_2700.mass, width = Ds1_2700.width,
                                        spin = Ds1_2700.spin, parity = Ds1_2700.parity, aboveLmin = 0,
                                        use_BLS_RealAndImag = True,
                                        couplings_dict_M = {
                                            "Ds1_2700__M_Bs1l0hall_Real" : Const(1.),  #FitParameter((Ds1_2700.name + "__M_Bs1l0hall_Real"), 0.5, 0., 1., 0.01),
                                            "Ds1_2700__M_Bs1l0hall_Imag" : Const(0.)}, #FitParameter((Ds1_2700.name + "__M_Bs1l0hall_Imag"), 0.5, 0., 1., 0.01)},
                                        couplings_dict_res = {"Ds1_2700__res_Bs0l2h-2_Real" : FitParameter("Ds1_2700__res_Bs0l2h-2_Real", 0.5, -10., 10., 0.01),
                                                              "Ds1_2700__res_Bs0l2h-2_Imag" : FitParameter("Ds1_2700__res_Bs0l2h-2_Imag", 0.5, -10., 10., 0.01),
                                                              "Ds1_2700__res_Bs0l2h0_Real"  : FitParameter("Ds1_2700__res_Bs0l2h0_Real", 0.5, -10., 10., 0.01),
                                                              "Ds1_2700__res_Bs0l2h0_Imag"  : FitParameter("Ds1_2700__res_Bs0l2h0_Imag", 0.5, -10., 10., 0.01),
                                                              "Ds1_2700__res_Bs0l2h2_Real"  : FitParameter("Ds1_2700__res_Bs0l2h2_Real", 0.5, -10., 10., 0.01),
                                                              "Ds1_2700__res_Bs0l2h2_Imag"  : FitParameter("Ds1_2700__res_Bs0l2h2_Imag", 0.5, -10., 10., 0.01)},
                                        resdecay_parity_conservation = True,
                                        colour = kBrown, linestyle = 7, fillstyle = 3954),

    # Lb --> Lc Ds1*(2860)
    #MatrixElement.BResonanceBreitWigner(name = Ds1_2860.name, label = Ds1_2860.label,
    #                                    fix_mass = Ds1_2860.mass, fix_width = Ds1_2860.width,
    #                                    spin = Ds1_2860.spin, parity = Ds1_2860.parity, aboveLmin = 0,
    #                                    use_BLS_RealAndImag = True,
    #                                    fitparams_BLS_Lb_real = [0.1, -2., 2.], fitparams_BLS_Lb_imag = [0., -2., 2.],
    #                                    fixBLS_res_toone = True,
    #                                    resdecay_parity_conservation = True,
    #                                    colour = kPink, linestyle = 7, fillstyle = 3954),    
]

# list of Lc K resonances
C = [
    # Lb --> Xic(2790) D0
    #MatrixElement.CResonanceFlatte(name = "Xic_2790", label = "#Xi_{c}(2790)", mass = Const(2.793), width = [Const(0.009), Const(0.01)],
    #MatrixElement.CResonanceBreitWigner(name = Xic_2790.name, label = Xic_2790.label,
    #                                    fix_mass = Xic_2790.mass,
    #                                    fix_width = Const(0.04),
    #                                    spin = Xic_2790.spin, parity = Xic_2790.parity, aboveLmin = 0,
    #                                    use_BLS_RealAndImag = True,
    #                                    fitparams_BLS_Lb_real = [0.1, -2., 2.], fitparams_BLS_Lb_imag = [0., -2., 2.],
    #                                    fixBLS_res_toone = True,
    #                                    resdecay_parity_conservation = True,
    #                                    colour = kCyan, linestyle = 7, fillstyle = 3954),
    
    # Lb --> Xic(2815) D0
    #MatrixElement.CResonanceFlatte(name = "Xic_2815", label = "#Xi_{c}(2815)", mass = Const(2.820), width = [Const(0.002), Const(0.003)],
    #MatrixElement.CResonanceBreitWigner(name = Xic_2815.name, label = Xic_2815.label,
    #                                    fix_mass = Xic_2815.mass,
    #                                    fix_width = Const(0.04),
    #                                    spin = Xic_2815.spin, parity = Xic_2815.parity, aboveLmin = 0,
    #                                    use_BLS_RealAndImag = True,
    #                                    fitparams_BLS_Lb_real = [0.1, -2., 2.], fitparams_BLS_Lb_imag = [0., -2., 2.],
    #                                    fixBLS_res_toone = True,
    #                                    resdecay_parity_conservation = True,
    #                                    colour = kCyan, linestyle = 7, fillstyle = 3954),

    # Lb --> Xic(3080) D0
    #MatrixElement.CResonanceBreitWigner(name = Xic_3080.name, label = Xic_3080.label,
    #                                    fix_mass = Xic_3080.mass,
    #                                    fix_width = Const(0.08),
    #                                    spin = 3, parity = -1, aboveLmin = 0,
    #                                    use_BLS_RealAndImag = True,
    #                                    fitparams_BLS_Lb_real = [0.1, -2., 2.], fitparams_BLS_Lb_imag = [0., -2., 2.],
    #                                    fixBLS_res_toone = True,
    #                                    resdecay_parity_conservation = True,
    #                                    colour = kCyan, linestyle = 7, fillstyle = 3954),
]

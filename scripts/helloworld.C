#include "RooRealVar.h"
#include "TFile.h"
#include <iostream>
#include "TTree.h"
#include "TCanvas.h"

using namespace RooFit;

int helloworld(){
    RooRealVar x("Hello World from Sebastian Neubert","from RooFit",-42,42);
    x.Print();
    
    // Try to open a file on EOS
    // This will only work if you have acquired a kerberos token via the anadat service account
    // See documentation for details
    TFile* f=TFile::Open("root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/Test/toy.root");
    if(f!=NULL)f->ls();
    else {
            std::cout << "File not found. Check if your job has access to eos" <<std::endl;
            return 1;
    }

    TTree* toy=(TTree*)f->Get("toy");
    TCanvas* c=new TCanvas("c","c",0,0,900,600);
    toy->Draw("x");
    c->SaveAs("x.png");

    return 0;
}
